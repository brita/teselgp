# Exploring Challenges in Avatar-based Translation from European Portuguese to Portuguese Sign Language #

A project to study and list open problems in a proposed a pipeline to translate from Portuguese text to Portuguese Sign Language using a 3D avatar.

The pipeline encompasses a Natural Language Processing and Translation step at the concept level, followed by a module to procedurally Synthesise an Animation of the resulting utterance.

http://web.ist.utl.pt/~ist163556/pt2lgp