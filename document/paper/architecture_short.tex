
%goals of the system and what it aims to be
\section{System Description} \label{sec:sysdesc}

The system's main goal is to translate from European Portuguese to Portuguese Sign Language.

The system should be able to accept isolated words and full sentences in text that are assumed to be correctly formed. For other use cases with different input, new conversion modules should be devised and coupled with the system.

We focus on an architecture that is be capable of synthesizing the gestures from defined subunits and logic placement of the hands.
We consider important that regular users may be able to insert new vocabulary in the system without any technical knowledge. It is, therefore, desirable that the gestures may be synthesized high-level descriptions and notations of sign language, but we do not focus further in designing an user interface for specifying the description.

The devised architecture should, as much as possible, be technology agnostic, as very different platforms (desktop, browser, mobile, vending machine\dots) are possible applications of a product deriving from this architecture.

A final and very important consideration is that all the system should be free as well as research and deployment friendly.



\section{Architecture} \label{sec:arch}

Figure \ref{fig:arch_1} presents the architecture overview, followed by an explanation of each component.

\begin{figure}[h]
	\centering \includegraphics[width=\columnwidth]{system/architecture_2}
	\caption{Proposed architecture}
	\label{fig:arch_1}
\end{figure}


\subsection{Gloss Translate}

%MT fos SLs follows current trends for spoken languages, without offering good explanations
Translation approaches to sign language tend to follow the contemporary trends used for spoken languages \cite{lse}.
These other works do not usually justify their choice and it is not clear if there is a preferable method.

%To choose between approaches, we need to weight the costs of building corpora and rules
To decide between \ac{SMT}, \ac{EBMT}, \ac{RBMT} and a hybrid approach, there is the need consider the cost of building the statistical or hand tailored data for the system to work with. The choice is dependent on the domain and task at hands.

%Corpora and linguistic studies
Building and using corpora is problematic matter for \acp{SL}, due to the difficulties of acquiring and storing the data. There is no standard writing or annotation system, or a way to easily process video or motion capture data into recognizable gestures.

To devise the system and to build a set of rules, there is also the need of linguistic studies and an understanding of the spatial grammar of the \ac{SL}. For \ac{LGP}, linguistic studies and references are scarce \cite{gramatica94,diclgp,gestuario}.

%We go with RBMT
Considering a non limited domain and the translation of full sentences, not just isolated words, we suggest a \ac{RBMT} approach.
%because..
We justify our option with the difficulties of using \ac{SL} corpora and a better control of the structural transfer between sentences, given the big grammatical gap between \ac{LGP} and Portuguese.

%figure and overview
In Fig. \ref{fig:transfer-trans} we present the overview of the \ac{RBMT} pipeline with a deep structural transfer and analysis until a semantic level to better cope with the gap between the languages. This way, knowing the dependencies and semantic roles (subject, object, verb complements\dots), the structure and proper agreement can be better inferred in the target sign language.
	\begin{figure}[h]
		\centering \includegraphics[width=\columnwidth]{system/transfer-trans_2}
		\caption{Overview of the translation pipeline}
		\label{fig:transfer-trans}
	\end{figure}


%tokenizing
The system starts by splitting the input text into sentences that are further tokenized into words and punctuation.

%pos tagging
Next, the tokens are parsed and POS-Tagged into a constituency tree. For the POS-Tagging, it is necessary a classified monolingual European Portuguese corpus.

%parsing and trees (dependency -> semantic rep)
The parsing into a constituency tree and, specially, into a dependency tree with verb as the root, allows the building of a semantic representation. % of the sentence, facilitating the structural transfer.

%information extraction
The result goes through several information extraction procedures, such as stemming, \ac{NER} and relation parsing. This information is fed into a semantic reasoning module to be further processed and passed to the animation stage.
The sentiment analysis serves the purpose of inferring subjective information towards entities and the generality of the sentence, so that emotional animation layers and facial expression reinforcement can be added to the result.
	
%transfer
Finally, the processed information can be used to accomplish the lexical transfer using a dictionary and the structural transfer using transfer rules and the semantic information. The semantic reasoning may need domain knowledge for gesture contextualization.





\subsection{Lookup}
The goal in this stage is to obtain a set of action ids for the animation from the sequence of already translated glosses.
For this to happen, the actions that represent each gesture need to be associated with its meaning, so as to be retrieved from the sequence of glosses obtained in the gloss translate.

The difficulty in designing this step is derived from the fact that many Portuguese words and concepts do not have a one-to-one matching in \ac{LGP}.
One word in Portuguese can also lead to different words in \ac{LGP} according to the context of the sentence.

It would be an advantage to be able to retrieve a gesture in \ac{LGP} from any given sign language notation, besides its gloss meaning, and
it would allow for more interoperability between sign language applications.
Figure \ref{fig:arch_lookup} shows an example of different gesture representations that relate to each other.


	\begin{figure}[h]
		\centering \includegraphics[width=\columnwidth]{system/architecture_lookup_2}
		\caption{Dictionary lookup for different representations of a gesture}
		\label{fig:arch_lookup}
	\end{figure}


How to retrieve gestures by its features in a database is also an interesting problem with ongoing research \cite{dicta10}.





\subsection{Animate}
%input-output
This stage receives a sequence of actions to be composed into a fluid animation, along with a set of hints on how best to do so, for example, if the gestures are to be fingerspelled, or are adjectives to the same concept.

%what this does
The animation stage is responsible for the procedural synthesis of the animation by blending gestures and gesture subunits together, but first, the actions matching each gesture need to be defined and the avatar needs to be set up.

% we build sentences from gestures and gestures from paramenters. procedimentally.
Given the high number of different gestures and gesture variability within sentences due to context, it is logic to place and modify the gestures procedurally, to match their usage within sentences, and also to build the gestures themselves.

%high level description to define gestures
We propose an approach where gestures are procedurally built and defined from an high-level description, based on the following parameters identified in other works \cite{liddell89, liddell03} as gesture subunits:
\begin{enumerate}[itemsep=1pt]
\item hand configuration,
\item hand orientation,
\item hand placement,
\item hand movement, and
\item non manual (facial expressions and body posture).
\end{enumerate}

This approach allows non animators to build gestures and community dictionaries.

%parameters have subjective definitions that can be dependent on the avatar porpotions/features
The base hand configurations are \ac{SL} dependent and identified for \ac{LGP} in \cite{diclgp,gramatica94,gestuario}.
The parameter definition for orientation, placement and movement is often of relative nature. For example, gestures can be signed `fast', `near', `at chest level', `touching the cheek' and so on.
The definition of speed is dependent on the overall speed of the animation, and the definition of locations is dependent on the avatar and its proportions.

We start by describing how to setup the character with a rig that has these issues in consideration.


\subsubsection{Rig} \label{sec:arch_rig}
To setup the character, we need an humanoid mesh with appropriate topology for animation and real-time playback and we need to further associate it with the mechanism to make it move, the \emph{rig}.
The rig is intrinsically associated with the procedural algorithms to pose the character and blend the poses into gestures and sentences.
We suggest a regular approach with a skinned mesh to a skeleton and bones.

The bones should be named according to a convention for symmetry and easy identification in the code.
For the arms and hands, the skeleton can approximately follow the structure of a human skeleton.
The rig ideally should have an \ac{IK} tree chain defined for both arms rooting in the spine and ending in the hands. All the fingers should also be separate \ac{IK} chains, allowing for precise posing of contacts.

The definition of the \ac{IK} chains is essential for being able to easily place the hands in the desired final place both procedurally and manually.
Ideally, the \ac{IK} chains should consider weight influence so that bones closer to the end-effector (hands and fingertips) are more affected, and the bones in the spine and shoulder nearly not so.
The rig should also provide a hook to control the placing of the shoulder.

The rig should make use of DOF, angle and other constraints for the joints so as to be easier to pose and harder to place in an inconsistent position when procedurally generating animation.
The definition of joints that mimic those of a human body is a difficult problem with suboptimal results.

%markers
Finally, the rig should have markers for placement of the hands in the signing space and in common contact areas in the mesh. These markers ensure that the character mesh and proportions can vary without breaking the animation and that gestures can be defined with avatar dependent terms (eg. `near', `touching the nose').

%grid
The markers in the signing space can be inferred automatically using the character's skeleton measures \cite{Kennaway2002, hamnosys}, forming a virtual 3D grid in front of the character as can be seen in Fig \ref{fig:grid}.

	\begin{figure}[h]
		\centering 
		\begin{subfigure}[b]{0.35\columnwidth}
		\includegraphics[width=\textwidth]{system/stupid-grid2}
		\end{subfigure}
		\begin{subfigure}[b]{0.35\columnwidth}
		\includegraphics[width=\textwidth]{system/stupid-grid3}
		\end{subfigure}
		%
		\begin{subfigure}[b]{0.35\columnwidth}
		\includegraphics[width=\textwidth]{system/stupid-grid4}
		\end{subfigure}
		\begin{subfigure}[b]{0.35\columnwidth}
		\includegraphics[width=\textwidth]{system/stupid-grid5}
		\end{subfigure}
		\caption{Virtual 3D marker grid defining the signing space in front of the character} 
		\label{fig:grid}
	\end{figure}

%spots
The markers in the mesh, however, need to be defined manually and skinned to the skeleton in a consistent manner with the nearby vertices. Figure \ref{fig:spots} shows a sample rig from our implementation, with key areas in the face and body identified by a bone with a position and orientation in space.

	\begin{figure}[h]
		\centering 
		\begin{subfigure}[b]{0.414\columnwidth}
 			\centering \includegraphics[width=\textwidth]{system/fem_cabeca}
		\end{subfigure}
		\hfill
		\begin{subfigure}[b]{0.5\columnwidth}
 			\centering \includegraphics[width=\textwidth]{system/fem_corpo4}
		\end{subfigure}
		\caption{Definition of key contact areas in the rig}
		\label{fig:spots}
	\end{figure}

We found no study that identifies key areas in the non dominant hand, only mentions that this hand is a very important articulating place, meaning that the dominant hand often contacts it in very precise places and ways (grasping, slapping, rubbing, stroking\dots).



\subsubsection{Building the gestures} \label{sec:building_gesture}

Having a working mechanism to pose the character, it is now necessary to record (\emph{key}) the poses in a good timing to build a gesture.
To accomplish this, there are two options: manually pose and keying using animation software, or acquire the data from motion capture and match it with the rig.

Whichever the keying methodology, all basic hand poses and facial expressions should be recorded and can then be combined given the high level description of the gesture.
The description should specify the gesture using the mentioned parameters: keyed hand configurations, placement and orientation using the spatial marks and movement also using the marks and the overall speed of the animation.

Building a gesture the is avatar independent is achieved by real-time modifying of the animation curves and rig posing.
The intersections defined by the grid from \ref{fig:grid}, in conjunction with marks from Fig. \ref{fig:spots} define the set of avatar relative locations where the hands can be placed.
Knowing the location where the hand should be, it can be procedurally placed with \ac{IK}, guarantying physiologically possible animation with the help of other constraints.
Figure \ref{fig:avatar-variability} shows the result of hand placement in a key area using two distinct avatars with significantly different proportions.

	\begin{figure}[h]
		\centering 
		\begin{subfigure}[b]{0.45\columnwidth}
 		\centering \includegraphics[width=\textwidth]{system/blenderella-coelho-spot}
		\end{subfigure}
		\hfill
		\begin{subfigure}[b]{0.45\columnwidth}
 		\centering \includegraphics[width=\textwidth]{system/fem-coelho-spot2}
 		\end{subfigure}
		\caption{Avatar variability within an action using the key areas} 
		\label{fig:avatar-variability}
	\end{figure}

While this approach works well for static gestures, several problems appear when introducing movement.
Gestures can change any of its parameters during the realization, requiring a blending from the first definition (of location, orientation, configuration\dots) to the second.
The type of blending is very important for the realism of the animation. Linear blending between two keys would result in robotic movement. The principles of animation, such as easing and anticipation need to be embedded in the animation curves.

Linear movement in space from one key location to another will also result in non realistic motion and even in
serious collision problems when, for example, making a movement from an ear to the other. This is a problem of arcs.
We found no study for this problem. Kennaway's research \cite{Kennaway2002,Kennaway2004,Kennaway2007,Kennaway2007a} mentions the possible need of collision handling.

Additionally, more movements need to be defined in order to accommodate other phenomena, such as finger wiggling and several types of hand waving.


\subsubsection{Blending the gestures} \label{sec:anim_synth}
Being able to build the individual gestures as needed for any given sentence, synthesizing the final fluid animation is now a matter of grammatically agreeing the gestures in space, of realistic interpolation of keys in time, similar to how it must be done within the building of gestures, and of blending actions with each other in a non-linear way.

%grammatical reasoning in space
The system needs a reasoning module, capable of placing gestures grammatically in the signing space, making use of the temporal line, entity allocation in space and other phenomena typically observed in \acp{SL} \cite{liddell03}.

%concatenating all the gestures, having curves
The interpolation between animation keys is given by a curve that can be modeled to express different types of motion. 
The individual actions for each gesture should be concatenated with each other and with a `rest pose' at the beginning and end of the utterance.

The animation curves should then be tweaked, following the principles of animation, easing in, defining arcs, and slightly exaggerating the defining keyframes as to better convey the expression of the gesture.

%counter animation and contacts
Counter animation and secondary movement is also very important for believability and perceptibility. For example, when one hand contacts the other or some part of the head, it is natural to react to that contact, by tilting the head (or hand) against the contact and physically receiving the impact. Besides the acceleration of the dominant hand, the contact is mainly perceived in how it is received, being very different in a case of gentle brushing, slapping or grasping.
This may be the only detail that allows distinguishing of gestures that otherwise may convey the same meaning.

%layering
Finally, actions need to be layered in the for expressing parallel and overlapping actions. This is the case for facial animation at the same time as manual signing and of secondary animation, such as blinking or breathing, to convey believability.
The channels used by some action may be affected by another action at the same time. The actions need to be prioritized, taking precedence in the blending with less important, or ending actions.

%npr rendering
NPR rendering  can be used to reinforce key parts of a gesture, by making use of the animation curves as input to the shaders. Blurs and stark line strokes in defining features of a gesture on important keyframes are suggested.




	\subsection{Running the System}

	Considerations on how and where the system will run should be taken throughout all the pipeline.

	From our research, we take that interesting applications of the system range from web navigation to television and public interfaces, considering also mobile devices in real life scenarios. The supported technologies for these platforms vary a lot and the processing power and physical memory are not high in most cases.

	For maximum portability, we suggest the OpenGL rendering subsets: \ac{GLES} and WebGL.
	A browser plugin, using web technology, would also be portable solution, though not a native mobile application or proper for set top boxes.

	A distributed architecture needs to be considered, where a client (the user's device) would make a request and receive the animation as reply from the server.
	The avatar(s) should typically be stored in the client side.
	As for the data (corpora, animation dictionaries\dots), if it is server-side, it can be shared and improved in a community effort, if it is client-side, the user is free to edit it and tailor if for his personal needs, as well as offline access.
