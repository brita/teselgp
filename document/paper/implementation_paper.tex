\section{Implementation} \label{sec:imp}
As proof of concept of our architecture and playground for experimentations, we implemented a prototype of the system.
The following sections describe our choices and particularities of the system.
The system can be downloaded\footcite{\url{http://web.ist.utl.pt/~ist163556/pt2lgp}}.

	\subsection{Blender} \label{sec:imp_blender}
	We chose Blender as the 3D package to use for modeling and animation.
	The main reasons for our choice are the quality and feature set of the 3D tools and that it is an open-source project with an also open community of developers and artists.
	
	Blender has a powerful and flexible API paradigm, that allows accessing and operating on all the data (animation, mesh\dots) via scripting.
	


	\subsection{Addon Philosophy} \label{sec:imp_addon}
	The choice of Blender as a modeling and animation tool implies some others, such as how to interface our system with the 3D model and the animation data and how to display the animation.

	Though it would be possible to implement a standalone system that interfaces with Blender, we saw no reason to deviate from Blender's addon philosophy. Blender offers a Python API for scripts to interact with the internal data structures, operators on said data, and with the interface. The interface can be customized with new panels and buttons though at the moment it does not allow the construction of entirely new widgets or editors.

	Blender also offers the infrastructure to easily share and install addons.
	
	With these considerations, the prototype was implemented as an addon, with all the logic, \ac{NLP} and access to the rig and animation data done in Python. The interface is a part of Blender using the pre-existing widgets and the avatar is rendered in real-time using the viewport renderer.



	\subsection{\acl{NLP} and Translation} \label{sec:imp_nlp}

	Following the choice of Python to implement the system's logic, we chose to use the \ac{NLTK}\wcite{nltk}\cite{nltkbook} for \ac{NLP} tasks.
	An overview of the implemented pipeline is shown in Fig \ref{fig:imp_nlp}.

		\begin{figure}[h]
			\centering \includegraphics[width=\columnwidth]{system/imp-nlp_2}
			\caption{Natural Language Processing pipeline used in the implementation}
			\label{fig:imp_nlp}
		\end{figure}
	
	\begin{itemize}
	\item \textbf{Error correcting and normalization} A step that enforces lowercase and the use of latin characters. Common spelling mistakes in the words used for the test cases are also corrected.

	\item \textbf{Tokenizer} The input string is split into sentences and then into words. The tokenizer, provided by \ac{NLTK}, uses Portuguese language training.
	Example of a tokenized input:
	\verb|['o', 'joão', 'come', 'a', 'sopa']|

	\item \textbf{POS-Tagger} We make use of \ac{NLTK}'s n-gram taggers, starting with a bigram tagger, with a backoff technique for an unigram tager and the default classification of `noun', being the most common class for Portuguese. We used the treebank `floresta sintá(c)tica' corpus \cite{floresta} for training the taggers.
	Using the same example, the result would be: \verb|[('o', 'art'), ('joão', 'prop'), ('come',| \verb|'v-fin'), ('a', 'prp'), ('sopa', 'n')]|
		
	\item \textbf{Named Entity Extraction} We apply \ac{NER} for proper names of persons by matching against a list of common Portuguese names. %Our system further supports a list of public personalities, such as the current Portuguese prime minister, with their matching gestural name. For these specific recognized entities, the system uses the known gesture instead of fingerspelling the name.
	
	\item \textbf{Stemmer} As a form of morphologic parsing, we apply a stemmer that identifies suffixes and prefixes to use as an adjective or classifier to the gloss. The result is treated as a whole. For example, the Portuguese word \emph{coelhinha}, roughly meaning `little female rabbit', is understood, by its suffix, to be a female and small derivation of the root \emph{coelh(o)}.

	\item \textbf{Lexical Transfer} The expanded and annotated list of words are converted to their corresponding glosses using a dictionary lookup. This works by referencing to the database described below, resulting in items such as \verb|['GLOSS', ['SOPA']]| and \verb|['FINGERSPELL', ['J', 'O', 'A', 'O']]|.

	\item \textbf{Structure Transfer}
	The prototype supports reordering of adjectives and quantities to the end of the affecting noun, for example the input \emph{dois coelhos} (`two rabbits') would result in \verb|[['GLOSS', ['COELHO']], ['NUMERAL', ['2']]|.
	
	The prototype also supports basic reordering of sequences of `noun - verb - noun', in an attempt to convert the \acs{SVO} ordering used in Portuguese to the more common structure of \acs{OSV} used in \acs{LGP}.
	\end{itemize}



\subsection{Lookup} \label{sec:imp_bd}
The animation lookup, given a gloss, is done via a JSON file mimicking a database.
This was thought to be the simplest and more portable solution for the data, without the strong need to recur to more solid ways of persistence in a proof of concept stage.

As to the design, the database consists of a set of \emph{glosses} and a set of \emph{actions}. The action ids are mapped to blender actions, that are in turn referenced by the glosses. One gloss may link to more than one action, that are assumed to be played sequentially. This design can be inspected in Figure \ref{fig:bd}, where it can be seen that \emph{coelho} (`rabbit') has a one-to-one mapping, that \emph{casa} (`house') corresponds to one action and that \emph{cidade} (`city') is a composed word, formed by \emph{casa} and a morpheme with no isolated meaning.

	\begin{figure}[h]
		\centering \includegraphics[width=0.95\columnwidth]{system/bd}
		\caption{Database design used in the implementation}
		\label{fig:bd}
	\end{figure}
	
	
	
\subsection{Animation} \label{sec:imp_anim}

	For the animation pipeline, we start by setting an avatar for use in the system by rigging and skinning. We chose \emph{rigify} as a base for the rig, that then needs to be extended with the spatial marks.

	All the 57 different hand configurations for \ac{LGP} were manually posed and keyed from references gathered from \cite{diclgp} and the Spread the Sign project.
	
	The animation is synthesized by directly accessing and modifying the action and f-curve data.

	We always start and end a sentence with the rest pose.
	For concatenating the actions, we \emph{blend} from one to the other in a given amount of frames by using Blender's \ac{NLA} tools that allow action layering.
	Channels that are not used in the next gesture, are blended with the rest pose instead.

	Figure \ref{fig:nla} illustrates the result for the gloss sentence `\verb|SOPA J-O-A-O COME|'.

		\begin{figure}[h]
			\centering \includegraphics[width=0.9\columnwidth]{system/nla}
			\caption{Action layering resulting of a translation}
			\label{fig:nla}
		\end{figure}
	
	We adjust the number of frames for blending according to the hints received. For fingerspelling mode, we expand the duration of the hand configuration (that is originally just one frame) and blend it with the next fingerspelling in less frames than when blending between normal gloss actions. We also expand this duration when entering and leaving the fingerspell.
	For the fingerspelling mode, we needed to directly access the fcurve data, as this can not be done with the NLA tools.


\subsection{Avatar} \label{sec:imp_avatar}
	Our implementation allows freedom in the choice of avatar, needing only that the character be imported into Blender and skinned to a \emph{rigify} armature.
	
	With the manual definition of key locations as explained in \refwra{sec:arch_rig}, avatar variability is supported.
	Throughout the development of the proof of concept, several characters were tested with success, with examples in Fig \ref{fig:avatars}. From the left to the right, there is \emph{Sintel} and \emph{Blenderella}, made freely available by the Blender Foundation, next, a model from the BlendSwap community\footnote{\url{http://www.blendswap.com/blends/view/13464}} where it is easy to search by rigify and freely licensed characters, and finally, the rightmost character, \emph{Catarina}, acquired by the \ldf group.
	A workflow using MakeHuman\wcite{makehuman} was also tested with success.

	\begin{figure}[h]
		\centering
			\begin{subfigure}[b]{0.23\columnwidth}
				\centering \includegraphics[width=\textwidth]{system/sintel2}
			\end{subfigure}
			\hfill
			\begin{subfigure}[b]{0.23\columnwidth}
				\centering \includegraphics[width=\textwidth]{system/blenderella}
			\end{subfigure}
			\hfill
			\begin{subfigure}[b]{0.23\columnwidth}
				\centering \includegraphics[width=\textwidth]{system/dude-crop}
			\end{subfigure}
			\hfill
			\begin{subfigure}[b]{0.23\columnwidth}
				\centering \includegraphics[width=\textwidth]{system/J}
			\end{subfigure}
		\caption{Example of supported avatars}
		\label{fig:avatars}
	\end{figure}



\subsection{User Interface} \label{sec:imp_ui}
The interface, in its most basic form, consists of an input text box, a button to translate, and a 3D view with the signing avatar. The 3D view can be rotated and zoomed, allowing to see the avatar from different perspectives.

The breakdown down in Fig.\ref{fig:ui} shows the main translation interface in blue.

	\begin{figure}[h]
		\centering \includegraphics[width=\columnwidth]{system/interface}
		\caption{User Interface for the prototype}
		\label{fig:ui}
	\end{figure}

Additionally, we provide an interface for exporting video of the signing (indicated in orange) and a short description of the project (in green).

It is still possible to use all the functionalities of Blender, thus making advanced usage of the system.



