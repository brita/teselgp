
%goals of the system and what it aims to be
\section{System Description} \label{sec:sysdesc}

The system's main goal is to translate from European Portuguese to Portuguese Sign Language. It encompasses a \ac{NLP} component and an animation synthesis step to accomplish this, that is further explained in the next section.

The system should be able to accept isolated words and full sentences, being assumed that they are in correct European Portuguese. For other use cases with different input, that were identified as useful in related work, new specific conversion modules should be devised for the task and coupled with the system. These modules are considered out of the scope of this work.

To present the results of the translation in signed form, we use 3D humanoid avatars.
The animation method must conform to what is affordable human work for the building of the lexicon in either a closed or open domain case.
To record with motion capture every possible gesture in \ac{LGP} was not considered feasible. The vocabulary of the language should round the hundreds of thousands of words, not taking into account agreement of the gestures inside a sentence. Therefore, we focus on an architecture that could be capable of synthesizing the gestures from defined subunits and logic placement of the hands.

We consider important that regular users may be able to insert new vocabulary in the system without any technical knowledge. It is, therefore, desirable that the gestures may be synthesized and corresponded to abstract, human readable, notations of sign language, such as the ones described in \refwra{sec:signwriting}.
We do not focus any further in the design and testing of an interface for gesture building and retrieval of existing gestures.

The devised architecture should, as much as possible, be technology agnostic, as very different platforms (desktop, browser, mobile, vending machine\dots) are possible applications of a product deriving from this architecture.

A final and very important consideration is that all the system should be free as well as research and deployment friendly. This means that the architecture should not oblige to the use of any payed or closed module that can not be modified, and that it should not hinder research and real-life deployment of the system.



\section{Architecture} \label{sec:arch}

Figure \ref{fig:arch_1} presents the architecture in a coarse level.
To achieve the final goal of translation from Portuguese to \ac{LGP}, a number of modules are necessary, defining a pipeline for data to flow.

\begin{figure}[h]
	\centering \includegraphics[width=1\textwidth]{system/architecture_1}
	\caption{Proposed architecture overview}
	\label{fig:arch_1}
\end{figure}



	\subsection{Input as text}
	The system receives input in form of text in Portuguese language. This input is assumed to be normalized and strictly correct, with no abbreviations, unrecognized words and characters, orthographic errors, or any other type of mistake.

	Before the data enters the system, other modules can be used to allow for good integration in different use case scenarios as long as they provide correct Portuguese text as output. Before passing the input to the system, it should be normalized and corrected according to its provenience.
	Figure \ref{fig:arch_text} illustrates this.

	\begin{figure}[h]
		\centering \includegraphics[width=.5\textwidth]{system/architecture_text}
		\caption{Diagram of the system input}
		\label{fig:arch_text}
	\end{figure}

	Examples of use cases are voice input, text from a picture taken with a mobile device, structured input coming from other applications, direct user input, SMS and other means of communication that are characterized by the use of abbreviations and incorrect spelling.

	%In this step, the words can be matched against a lexicon of terms that the system is capable of handling

	\subsection{Gloss Translate}
	Translation approaches to sign language tend to follow the contemporary trends used for spoken languages \cite{lse}. At first, most works used \ac{RBMT} approaches, passing then to \ac{SMT} and machine learning.
	These other works do not usually justify their choice and it is not clear if there is a method preferable to the others. Therefore,
	we do not enforce a particular approach in our architecture.

	For a \ac{SMT} approach, statistical data is needed, namely a language model for \ac{LGP} and a bilingual lexicon and alignment model.
	
	One of the main identified problems for sign language \ac{NLP} is the lack of existing corpora and the difficulty of building them due to the modality of the language. This means that, because sign languages are visual and have no standard for a writing system, there are no comprehensive, good quality and appropriately descriptive corpora. This is further explained in \refwra{sec:problems_sl}.
	
	At the moment, to the best of our knowledge, there are only video databases for \ac{LGP}, that are either proprietary or incomplete.

	If the \ac{SMT} method is to be used, there is the need to first acquire and store the data in sufficient amounts to build the models. The viability of this option depends on the domain and task at hands.
	
	For a \ac{RBMT} approach, at least the lexicon model or a bilingual dictionary need to be built, the difference being that a bilingual dictionary is hand built and a lexicon model is statistically built from aligned corpora, counting the probability of one word being translated into an other by using n-grams.
	
	
	To decide between \ac{SMT} and \ac{RBMT}, one needs to consider the cost of building the statistical or hand tailored data for the system to work with and to study further the \ac{LGP} linguistic characteristics so as to understand the level of matching between the lexicon and grammar of this pair of languages. Only with more research it is possible to determine the best approach.
	
	While hand building a bilingual dictionary is a more deterministic and controlled approach, it also requires a lot of work and a decision on how to represent a gesture in written form.
	
	The \ac{RBMT} approach also needs a way to know how to align the structure of the target sentence and how to proceed when there is not a one-to-one word correspondence in the lexicon or dictionary. For this, grammatical transfer rules need to be built, being this a task for linguists that have an in depth knowledge of both languages of the pair.
	
	Figure \ref{fig:transfer-trans} shows an overview of a \ac{RBMT} pipeline.
	\begin{figure}[h]
		\centering \includegraphics[scale=0.8]{system/transfer-trans}
		\caption{Overview of a rule-based approach for the translation pipeline}
		\label{fig:transfer-trans}
	\end{figure}

	The system starts by splitting the input text into sentences that are further tokenized into words and punctuation.

	Next, the tokens are parsed and POS-Tagged into a constituency tree, as shown in the left side of Fig. \ref{fig:no-se-aburre}. For the POS-Tagging, it is necessary a classified monolingual European Portuguese corpus.
	
	The parsing into a constituency tree and, specially, into a dependency tree such as the one shown in the right side of Fig. \ref{fig:no-se-aburre} is a very important part of the pipeline. With the dependency representation, having the verb as the root, it is possible to build a semantic representation of the sentence.
	The process of structural transfer is also facilitated.

	\begin{figure}[h]
		\centering \includegraphics[width=0.8\textwidth]{papers/lse_solo-no-se-aburre}
		\caption{Example of a constituency tree and a dependency tree built for the same sentence \cite{lse}}
		\label{fig:no-se-aburre}
	\end{figure}

	The result goes through several information extraction procedures, such as stemming, \ac{NER} and relation parsing. This information is fed into a semantic reasoning module to be further processed and passed to the animation stage.

	Figure \ref{fig:ner} shows types of entities considered by \ac{NER} that need different types of processing. For example, a Person or Animal name receive very similar treatment in \ac{LGP}, by being referenced to by their gestural name if known or fingerspelt otherwise. A location is similar, but is more likely to have an assigned official gesture. Both need to be properly agreed with in the signing space, if used within a sentence. This is likely a process that is different for all cases, but there are no \ac{LGP} linguistic studies that specify how.

	\begin{figure}[h]
		\centering \includegraphics[scale=0.8]{system/ner}
		\caption{Diagram of classes recognized by Named Entity Extraction and their treatment}
		\label{fig:ner}
	\end{figure}

	The relation extraction step helps defining the relation between the entities so as to have more information for agreeing the sentence in \ac{LGP}.

	We argue that with the grammatical gap between any sign language and its spoken counterpart, the translation results are better if the analysis of the source sentences goes until a semantic level. This way, knowing all the dependencies and semantic roles (subject, object, complements of time \dots), the structure and proper agreement can be better inferred in the target sign language.

	The semantic reasoning may need domain knowledge and can use sentiment analysis on the recognized entities and relations to extract more information.
	The sentiment analysis serves the purpose of being able to understand some of the modality of the actions in the sentence, so that the verb can be better agreed on aspect, and the purpose of acting as input for the avatar's emotions and facial expression reinforcement that are very important in a sign language. 


\subsection{Lookup}
In the `Lookup' stage, the goal is to obtain a set of action ids for the animation from the sequence of already translated glosses.
The `Animate' stage, next in the pipeline, will be responsible for further processing the actions and blending them into one fluid animation for presenting.

For this to happen, the actions that represent each gesture need to be associated with its meaning, so as to be retrieved from the sequence of glosses obtained in the gloss translate.

The difficulty in designing this step
%is due to there being no one-to-one matching in the lexicon of the two languages. 
is derived from the fact that many Portuguese words and concepts do not have a one-to-one matching in \ac{LGP}.
A concept from Portuguese can generate one, more than one or none word in \ac{LGP}. To the notion of how many words in the target language are generated from one word in the source language, is called fertility.
One word in Portuguese can also lead to different words in \ac{LGP} according to the context of the sentence.
The design of the dictionary and the lookup need to take all this into account.

Additionally,
it is desirable that a maximum of different information is able to be extrapolated from the same database, besides a one directional translation.

Figure \ref{fig:arch_lookup} shows an example of different gesture representations that relate to each other. %, not necessarily in a one to one mapping.

	\begin{figure}[h]
		\centering \includegraphics[scale=.8]{system/architecture_lookup}
		\caption{Dictionary lookup for different representations of a gesture}
		\label{fig:arch_lookup}
	\end{figure}

It would be an advantage to be able to retrieve a gesture in \ac{LGP} from any given sign language notation, besides its gloss meaning, and
it would allow for more interoperability between sign language applications.

The mapping between each gesture and each of the gesture notation systems is, again, not linear and still to be determined for \ac{LGP}.

How to retrieve gestures by its features in a database is still an open problem.



\subsection{Animate}
%input-output
The `Animate' step receives a sequence of actions to be composed into a fluid sentence. This stage also receives a set of hints on how best to do so, for example, if the gestures are to be fingerspelled, or are adjectives to the same concept, or the verb of the sentence.
The hints are generated by the structural transfer step and also by the semantic reasoning that passes along extracted information, such as recognized entities and the sentiment analysis on top of those entities.

%what this does
The animation stage is responsible for the procedural synthesis of the animation by blending gestures and gesture subunits together, but first, the actions matching each gesture need to be defined and the avatar needs to be set up.

% we build sentences from gestures and gestures from paramenters. procedimentally.
Given the high number of gesture variability within different sentences due to context, and the high number of existing gestures (in the order of hundreds of thousands), it is logic that we choose to place and modify the gestures procedurally, to match their usage within sentences, and also to build the gestures themselves.

In \refwra{sec:signwriting} and in most of the related work, we see that it is given that gestures may be built up from smaller units or parameters and described accordingly, although good methods for doing so are still being researched.

We propose an approach where gestures are defined from an high-level abstraction, such as one of the notations in \refwra{sec:signwriting} that is capable of precisely physically describe any gesture in \ac{LGP}. We exclude, therefore, the Stokoe notation.

The advantage of this approach would be to allow non animators to build gestures from an user-level description, either by using one of the above notations or a custom interface to build the gestures. The system should then be capable of composing a gesture from the given high-level indications and of further customizing the gesture for use within a sentence.

The identified parameters that can be customized are:
\begin{enumerate}
\item hand configuration,
\item hand orientation,
\item hand placement,
\item hand movement, and
\item non manual (facial expressions and body posture).
\end{enumerate}

The hand configuration is one of the present in \refwraa{app:hand_configs}.
There are works that defend that \ac{LGP} needs less configurations \cite{gramatica94}, because, for example the configurations for `N' and `U' only differ in orientation, and others that defend the need of many more base configurations to have proper realism and accuracy \cite{hamnosys}.

For hand orientation and placement, different words describe different subdivisions and precisions and it is clear that further study is needed for \ac{LGP} in order to define the possibilities of the language and design the system accordingly.

The manual movement and non manual parameters, such as head, torso movement and facial expressions, are by far the most complicated to understand and animate and need intensive research and collaboration from the deaf community for proper modeling of the language and grammar.

For animation, it is important to understand what are the defining parameters and what is mere accessory movement for any given gesture. For example, there are cases where only the hand configuration matters, and orientation or placement do not. In one handed signs, the non dominant hand is `just there', such as the head, as opposed to some other gestures where the head needs to be in a certain way to convey the proper meaning.

The parameter definition is often of relative nature. For example, gestures can be signed `fast', `near', `at chest level', `touching the cheek' and so on.
The definition of speed is dependent on the overall speed of the animation, and the definition of locations is dependent on the avatar and its proportions.

We start by describing how to setup the character with a rig that has these issues in consideration.

\subsubsection{Rig} \label{sec:arch_rig}
To setup the character, one needs an humanoid mesh with appropriate topology for animation and real-time playback and needs to further associate it with the mechanism to make it move: the \emph{rig}.

The rig is intrinsically associated with the procedural algorithms to pose the character and blend the poses into gestures and sentences.

We suggest a traditional approach to character digital animation with a skinned mesh to a skeleton and bones.
There are no precise requirements for the skeleton definition, except for the arms, hands and facial animation. The bones should follow proper naming conventions for symmetry and easy identification in the code.

For the arms and hands, the skeleton can follow more or less closely the structure of a human skeleton.
The rig ideally should have an \ac{IK} tree chain defined for both arms rooting in the spine and ending in the hands. All the fingers should also be separate \ac{IK} chains, allowing for precise posing of contacts.

The definition of the \ac{IK} chains is an essential step for being able to easily place the hands in the desired final place both procedurally and manually.
Ideally, the \ac{IK} chains should have weighting of the influence along the chain so that bones closer to the end-effector are more affected, and the bones in the spine and shoulder nearly not so.
The rig should provide some form of hook to control the placing of the shoulder if needed.

The joints in the arms and fingers should have their \ac{DOF} and maximum angles constrained so as to be easier to pose and harder to get automatically in an impossible position when generating animation.

The definition of joints that mimic those of a human body is a difficult problem with suboptimal results.

\vspace{0.5cm}
For easy placement of the hands, the rig should have special indicators in parts of the mesh commonly used for contact.

The common regions in the signing space, in front of the character, do not need to be specified manually, as the algorithm can later infer a grid that is proportional to the character's arms using the skeleton measures.
Places of \emph{contact}, though, need to be specified for each mesh according to the character's facial and body features so as to avoid self collisions.
%or the character will poke itself in the eye

The common places of contact in the face and body as identified in the complete work of \citeauthor{liddell89} for \ac{ASL} can be seen in \refwraa{app:contacts_asl}.
The same study was also done for \ac{LGP}, but for a much smaller study group. The resulting identified areas are present in \refwraa{app:contacts_lgp}.
We found no study that clearly identifies key areas in the non dominant hand, only mentions that this hand is a very important articulating place, meaning that the dominant hand often contacts it in very precise places and ways (grasping, slapping, rubbing, stroking\dots).

Figure \ref{fig:spots} shows a sample rig (the one used in our implementation), with key areas identified by a bone with a position and orientation in space.

	\begin{figure}[h]
		\centering 
		\begin{subfigure}[b]{0.45\textwidth}
 			\centering \includegraphics[width=0.75\textwidth]{system/fem_cabeca}
		\end{subfigure}
		\quad
		\begin{subfigure}[b]{0.45\textwidth}
 			\centering \includegraphics[width=0.9\textwidth]{system/fem_corpo4}
		\end{subfigure}
		\caption{Definition of key contact areas in the rig for the face and body}
		\label{fig:spots}
	\end{figure}


The tagging of these areas in the rig, relative to the specific mesh, is very important both for character variability of proportions and features and for the automatic definition of the gesture, as explained below in \refwra{sec:building_gesture}.

\vspace{0.5cm}

As for non-manual animation, a dedicated set of controls is needed to control facial features in order to display facial reinforcement and to convey mood and character believability.
Facial reinforcement and mouthing are sometimes mandatory parameters of a gesture or grammatical constructions, for example, displaying an inquisitive look with the eyebrows and head tilt forward to pose a question.

As a minimum requirement, a set of controls for eyebrows, eye blinking and eye gaze, as well as a set for mouth shapes, is required.
Such controls can use the skinning technique previously described, but can also rely on shape keys, a set of pre recorded mesh displacements - similar to poses, but in the context of the character's mesh - recalled on demand during the animation process.


\subsubsection{Building the gestures} \label{sec:building_gesture}
Having a working mechanism - the \emph{rig} - to build a gesture, it is now necessary to pose the character and record - \emph{key} - the poses in a good timing.

To accomplish this, there are two options: to manually pose and key the rig using animation software, or to acquire the data from motion capture.

Manual posing and keying is a much more precise method that requires the work of expert animators. The quality of the animation is dependent on the quality of the animator and of the references he has available. The animator should follow the principles of animation and use every tool to achieve believable animation inside a gesture.
However, as it is not possible to manually key all possible gestures and their spatial agreement within a sentence, the gestures need to be carefully built so that they can be later modified, merged into more complex gestures and concatenated into a sentence.

The animator should only key the bones and channels that are necessary to the definition of the animation, leaving all others for the procedural animation algorithm. For example, for most hand configurations, only the hand orientation and the fingers should be keyed, leaving the rest of the character and even the hand placement free to be animated by other mechanisms.
The animator should also make use of automatic inbetweens that the system can adjust later.

Manual posing and keying lends itself well to the necessary care for later blending the gestures.
Motion capture, on the other hand, requires significant effort in processing the acquired data, with the advantage of quickly yielding much more realistic results.

Current technology for motion capture allows this technique to be applied in various ways in order to animate a character instead of manual keying.

The first possibility is to use precise hardware, such a suit and gloves, and to have signing deaf persons perform the gestures to be added to the vocabulary.

A second possibility is the use of more practical hardware capturing devices such as Leap\footnote{\url{https://www.leapmotion.com}}. The German research group for Sign Language Synthesis and Interaction\footnote{\url{http://slsi.dfki.de}} has several publications\footnote{\url{http://slsi.dfki.de/publications}} and a working system to detect sign language using LEAP and a Kinect to capture facial expressions and additional information.

The last possibility is to use video footage directly using motion tracking as shown in Fig. \ref{fig:sebastian}. However, the blur produced when the hands move too fast still constitutes a problem that implies a human-supervised approach to the tracking. This is a fast advancing technology and more improvements are expected in Blender thanks to the work of Keir Meirle\footnote{\url{http://ceres-solver.org}}, Sergey Sharybin and Sebastian König.

	\begin{figure}[h]
		\centering \includegraphics[width=0.7\textwidth]{anim/sebastian3}
		\caption{Motion Tracking from video footage using Blender} %König in his training video on point tracking
		\label{fig:sebastian}
	\end{figure}

Although motion capture yields realistic animation relatively fast, it is still a labor intensive work, that requires a significant setup cost and further research on to handle the data.

After acquiring the data from movements performed by human signers, the data needs to be treated to eliminate noise and be in a state liable to be applied to the skeleton. The actual gestures need to be isolated from non meaningful rest moments and secondary movements. The data may need down sampling to be manageable by an algorithm.

There is very little research on how to adapt this data to build or modify a gesture, for example, placing a hand in a certain configuration in an arbitrary place and orientation.

Until works on how to handle procedurally the huge amount of data that results from motion capture and on gesture subdivision in recognized parameters evolve to a more stable state, gestures would have to be performed for every word in the vocabulary, in every possible context as there is no way to extract and reuse gesture parameters by blending and combining.

%It is also difficult to incrementally add vocabulary once the setup is lost.

%This keying approach has the disadvantage of setup and the cost of having several deaf signers performing a large vocabulary of gestures. The huge amount of retrieved data can be very difficult to manage and blend between gestures.
%However, motion capture provides a greater level of realism to the animation and can result in a much better output, provided that research is made in order to reuse sub gesture parameters or that the system is employed in a limited domain, where the human signers can provide all the lexicon.

%The gestures should be performed several times and by different interpreters to account for inter- and intra- person variability.

\vspace{0.5cm}
Whichever the keying methodology, all basic hand poses and facial expressions should be recorded and can then be combined given an high level description of the gesture.

The basic idea for manual movements is to pick a hand configuration and place it and orient it according to the description, for example: `near', `at chest level' or `from the non dominant side to the dominant side'.

For this, an avatar variant grid corresponding to the signing space needs to be built, so as to determine possible places where to place the hand.
Knowing the location where the hand should be, it can be procedurally placed with \ac{IK}, guarantying physiologically possible animation with the help of other constraints.

Lacking more specific \ac{LGP} research, we follow \cite{Kennaway2002, hamnosys} in the definition of the grid for the signing space that we illustrate in Fig. \ref{fig:grid}. The grid is defined as 4 levels high, 5 levels wide and 3 deep.
The heights are defined relative to \emph{s - shoulder}, \emph{e - elbow} and \emph{w - wrist} by $s$, $(s+e)/2$, $e$ and $(e+w)/2$. The five widths are defined symmetrically, at shoulder level, in the center and halfway. The depth is defined at its furthest at the arm's length.

	\begin{figure}[h]
		\centering 
		\begin{subfigure}[b]{0.34\textwidth}
		\includegraphics[width=\textwidth]{system/stupid-grid2}
		\end{subfigure}
		\begin{subfigure}[b]{0.34\textwidth}
		\includegraphics[width=\textwidth]{system/stupid-grid3}
		\end{subfigure}
		%
		\begin{subfigure}[b]{0.34\textwidth}
		\includegraphics[width=\textwidth]{system/stupid-grid4}
		\end{subfigure}
		\begin{subfigure}[b]{0.34\textwidth}
		\includegraphics[width=\textwidth]{system/stupid-grid5}
		\end{subfigure}
		\caption{Grid defining the signing space in front of the character} 
		\label{fig:grid}
	\end{figure}

The intersections defined by the grid, in conjunction with the key areas from Fig. \ref{fig:spots} define the set of avatar relative locations where the hands can be placed.
Figure \ref{fig:avatar-variability} shows the result of hand placement in a key area using two distinct avatars with significantly different proportions.

	\begin{figure}[h]
		\centering 
		\begin{subfigure}[b]{0.45\textwidth}
 		\centering \includegraphics[width=0.7\textwidth]{system/blenderella-coelho-spot}
		\end{subfigure}
		\quad\quad
		\begin{subfigure}[b]{0.45\textwidth}
 		\centering \includegraphics[width=0.7\textwidth]{system/fem-coelho-spot2}
 		\end{subfigure}
		\caption{Avatar variability within an action using the key areas} 
		\label{fig:avatar-variability}
	\end{figure}

While this approach works well for static gestures, several problems appear when introducing movement.
Gestures can change any of its parameters during the realization, requiring a blending from the first definition (of location, orientation, configuration\dots) to the second.

The type of blending is very important for the realism of the animation. Linear blending between two keys would result in robotic movement. Some of the principles of animation, such as easing and anticipation need to be embedded in the animation curves.

Linear movement in space from one key location to another will also result in non realistic motion and even in
serious collision problems when, for example, making a movement from an ear to the other. This is a problem of arcs.

We found no study for this problem. Kennaway's research mentions the possible need of collision handling, but he does not investigate how to do so.

We can advance that to key a path from each key location to all the others approximates a quadratic growth, specifically, for $n$ key locations, the number of paths is the triangular number given by $(n-1) + (n-2) + \dots + (n-n) = n(n-1)/2$. Meaning that, while not impossible to do manually, it is certainly a tedious task.
In our implementation we defined approximately 100 key locations without regarding the hands themselves or the grid where a linear path is acceptable to most parts of the avatar.

Having a path defined, using either a solver or manually keyed paths, the curve can then be adjusted in time, such as all other action curves.

More movements need to be defined in order to accommodate other phenomena, such as finger wiggling and several types of hand shaking.



\subsubsection{Blending the gestures} \label{sec:anim_synth}
Being able to build the individual gestures as needed for any given sentence, to synthesize the final fluid animation is now a matter of grammatically agreeing the gestures in space, of realistic interpolation of keys in time, similar to how it must be done within the building of gestures, and of blending actions with each other in a non-linear way.

\vspace{0.5cm}
Gestures need to be properly positioned and orientated in the signing space for correct grammatical agreement. Figures \ref{fig:agree1} and \ref{fig:agree2} from \ac{ASL} studies illustrate the grammatical usage of signing space.

The system needs a reasoning module, capable of placing gestures in the timeline and entities in their already allocated spaces or in new ones if not mentioned before.

\vspace{0.5cm}
The interpolation from one key to another is given by a curve that can be modeled to express different types of motion. Figure \ref{fig:easings} shows an example of different curves that result in different motion. The curves are expressed with value (vertical axis) in function of time (horizontal axis).

	\begin{figure}[h]
		\centering 
		\begin{subfigure}[b]{0.34\textwidth}
		\includegraphics[width=\textwidth]{system/linear}
		\caption{linar}
		\end{subfigure}
		\quad\quad
		\begin{subfigure}[b]{0.44\textwidth}
		\includegraphics[width=\textwidth]{system/accel}
				\caption{accelerated}
		\end{subfigure}
		%
		\begin{subfigure}[b]{0.34\textwidth}
		\includegraphics[width=\textwidth]{system/ease}
				\caption{ease in and out}
		\end{subfigure}
		\quad\quad
		\begin{subfigure}[b]{0.44\textwidth}
		\includegraphics[width=\textwidth]{system/deaccel}
				\caption{deceleration}
		\end{subfigure}
		\caption{Animation curves defining different types of motion} 
		\label{fig:easings}
	\end{figure}

The individual actions for each gesture should be concatenated with each other and with a `rest pose' at the beginning and end of the utterance.
There should be an interpolation curve connecting each key of the same channel at appropriate timing.

The curves for each key should then be tweaked as exemplified in Fig. \ref{fig:easings}. For the animation we are aiming to achieve, the curves should never be linear. They should follow the principles of animation, easing in and out, defining arcs, and slightly exaggerating the defining keyframes so as to better convey the expression of the gesture.

Counter animation and secondary movement is also very important, for example, when one hand contacts the other or some part of the head, it is natural to react to that contact, by tilting the head (or hand) against the contact and physically receiving the impact.
This is very important for the perceiving of the type of contact.
For example, when one hand touches the other, besides the acceleration of the dominant hand, the contact is mainly perceived in how it is received, being very different in a case of gentle brushing, slapping or grasping.
This may be the only detail that allows distinguishing of gestures that otherwise may convey the same meaning.

Anticipation is also key in the realism of the animation, being taken to an extreme in signing, that is called \emph{coarticulation}. This is the name given to the influence that a gesture has on the preceding one, because we, as humans, are already thinking of it when signing the current gesture. This was a phenomenon first observed in speech.


If the idea from \cite{deusdado02} of \ac{NPR} line rendering to reinforce key parts of a gesture is used, it can be further enhanced here by using the curves as an input for rendering, making the strokes in the hands and facial expression more stark in the important keyframes and less so on transitional ones.

\vspace{0.5cm}

Finally, some actions need to be layered in the same timing for expressing parallel actions. This is the case for facial animation at the same time as manual signing and of secondary animation, such as blinking or breathing, to convey believability.

The channels used by some action may be affected by another action at the same time. The actions need to be prioritized, taking precedence in the blending with less important, or ending actions.

\vspace{0.5cm}

Only the consideration of all these factors can result in polished animation. To note however, that animation is a very difficult subject that requires art, making it yet more difficult to procedurally generate. It is necessary to research on how best to implement the mentioned factors, specially to determine what are the ones important to convey meaning as an higher priority than realism.




	\subsection{Display}

	Considerations on how and where the system will run should be taken throughout all the pipeline.

	With information taken from the related work and informal conversations with elements of the Portuguese deaf community, interesting applications range from using the system for web navigation, to real life scenarios via mobile devices, and public interfaces such as vending machines and public television.

	From this, we take that the platforms and their supported technologies vary a lot and that the processing power and physical memory are not high in most cases.

	For maximum portability, we suggest the OpenGL rendering subsets: \ac{GLES} and WebGL.

	A browser plugin, using web technology, would also be portable solution, though not a native mobile application or proper for set top boxes.

	A distributed architecture needs to be considered, where a client (the user's device) would make a request and receive the animation as reply from the server.
	
	One approach is to keep all the data and logic running on the server side. The system acts upon request, receiving the input text, and returns the animation curves for the avatar. All data and logic are server-side, the client merely renders the animation real-time.
	
	Another approach is to have more data and logic on the client side. This can allow, for example offline usage. 
	For the logic to run on the client side, the technology is platform dependent.

	The avatar(s) should typically be stored in the client side.
	As for the data (corpora, animation dictionaries\dots), if it is server-side, it can be shared and improved in a community effort, if it is client-side, the user is free to edit it and tailor if for his personal needs, as well as the already mentioned offline access.

	Data to consider where and how to store is:
	\begin{itemize}
	\item avatar (mesh, materials and textures) and 3d scene (lighting setup, world..),
	\item corpora,
	\item dictionary database with gestures.
	\end{itemize}\

	Logic to consider where and in what technology to run:
	\begin{itemize}
	\item \ac{NLP} and translation,
	\item animation synthesis,
	\item animation solver for real-time playback.
	\end{itemize}