\section{Description Systems for Sign Languages} \label{sec:signwriting}

Systems for written description of sign language are important for several tasks, one of them being computer processing.

Notations can range from attempting to represent the meaning of an utterance to a physical representation of the signing. They can also focus on every day usage by regular persons or in specialized usage by linguists and works such as this.

The systems that we present next can be used for annotation of corpora and transcriptions to be used as input for \ac{NLP}.
All methods have limitations.

However important a writing system for sign language would be, there isn't a worldwide proven standard for users or for computer processing.


\subsection{Gloss} \label{sec:gloss}

The gloss notation is focused on the \emph{meaning} of the gestures.
Glosses (from the greek, meaning `language' such as in `glossary') are side notes on the meaning of something.
In the case of \acp{SL} they are seen in all caps english words following below or above the original sentence. Figure \ref{fig:gloss} shows an example, where $PRO-1$ indicates a personal pronoun in the first person.

\begin{figure}[h]
	\centering
	$ [PRO-1 \quad LIKE]^{NEGATIVE} \quad [WHAT?]^{RETHORICAL} \quad SOUP $
	\caption{Example of a gloss notation for the sentence ``I don't like soup''}
	\label{fig:gloss}
\end{figure}

Fingerspelling is indicated by hyphenation.

\subsection{Stokoe Notation} \label{sec:stokoe}

William Stokoe was the first to propose, in 1960, that signs have an internal structure equivalent to the phonetic level of vocal languages\cite{Stokoe2005}.
This was his strong argument towards affirming that \acp{SL} are full natural languages, since they were not considered so at the time.

Stokoe coined the terms \emph{chereme} and \emph{cherology} so as not to use the morpheme `\emph{phono}', from the greek and meaning `sound'  or `voice'.

In Stokoe's model, signs have 3 aspects with terminology also coined by him: tab (tabula, place of articulation or simply placement), dez (from the word designator for hand configuration) and sig (the movement to produce the signal).
The three aspects are simultaneous, and the movements (sig) are then sequential.
Gestures have a single oriented hand configuration (dez), a single placement (tab) and one or more movements (sig).
Movements can be along a path, of the wrist, interactions and contacts, or affecting hand configuration such as wiggling fingers, totaling 24 different possible movements.
Hand orientation is represented with a diacritic to the dez, if represented at all. Non-manual features are not represented.

The model is denoted mostly by latin characters aiming for ease of use.

Stokoe co-authored the first dictionary of \ac{ASL} using his notation for all entries in addition to pictures. An example of an entry is shown in Fig \ref{fig:stokoedic}.

	\begin{figure}[h]
		\centering \includegraphics[width=0.25\textwidth]{sl/stokoe-dict}
		\caption{Sample entry from the first dictionary of \ac{ASL} using Stokoe Notation}
		\label{fig:stokoedic}
	\end{figure}

It is interesting to note that Stokoe's dictionary was keyed by the dez of the sign, and not by alphabetic order of the meaning in american-english.

The model was concluded to be enough to distinguish \ac{ASL} gestures, as it relies on the fact that not all combinations are possible in \ac{ASL} and thus, users could disambiguate the notation for only existing signs. 
Stokoe proposed 19 different hand configurations (dez), which are also not enough to precisely detail possible signs.
The model does not contain enough detail to go from notation to production.\cite{liddelljo1}

The notation did not achieve significant usage by the deaf.



\subsection{HamNoSys} \label{sec:hamnosys}
HamNoSys \cite{hamnosys} stands for Hamburg Sign Language Notation System and is a phonetic transcription system, meaning that it represents the signing by its physical characteristics.

HamNoSys was developed for a scientific project as a tool to annotate sign language in conjunction with signing avatars and gesture recognition and parameter extraction.
It was never intended for regular persons to use in their daily lives.


This system recurs to a series of 210 highly iconic symbols, representing hands, placements, movement, etc for representing a sign. The symbols are provided as an unicode-based character set for free.
Figure \ref{fig:hamnosys} displays, on the right, the possible hand configurations and orientations. On the left, the same figure displays placement indicators.
HamNoSys defines 60 positions in the signing space in front of the signer, arranged in a grid.
The grid is divided in 5 levels from the left to the right of the signer, in 4 levels for the shoulder, chest, abdomen and below the waist. The depth is defined by 3 levels, from near to far.
In the four bottom rows of the table on the right side of Fig. \ref{fig:hamnosys} can be seen the symbol constructions corresponding to this grid.

	\begin{figure}[h]
		\centering
			\begin{subfigure}[b]{0.4\textwidth}
					\centering\includegraphics[width=\textwidth]{papers/the-one}
					\caption{location indicators}
			\end{subfigure}
			\quad\quad
			\begin{subfigure}[b]{0.5\textwidth}
				\begin{subfigure}[b]{\textwidth}
					\centering\includegraphics[width=.9\textwidth]{papers/basichamnosys}
					\caption{basic hand shapes}
				\end{subfigure}

				\begin{subfigure}[b]{\textwidth}
					\centering\includegraphics[width=.9\textwidth]{papers/hamnosys2}
					\caption{hand orientations}
				\end{subfigure}
			\end{subfigure}
			\caption{Some of the symbols for HamNosys}
		\label{fig:hamnosys}
	\end{figure}

HamNoSys further supports facial expressions and mouthing.
The notation is international, being used and tested for different sign languages.
It exceeds the Stokoe notation in terms of structure and expressive power.


One of the identified problems of HamNoSys is that one same gesture can be written in different ways and that some are ambiguous for avatar reproduction, and some are not \cite{Kennaway2004}. This fact is illustrated in Fig. \ref{fig:hamnosys-visicast}.

	\begin{figure}[h]
		\centering \includegraphics[width=0.6\textwidth]{papers/hamnosys_visicast}
		\caption{Example of bad and good notations of the same sign using HamNoSys \cite{Kennaway2004}}
		\label{fig:hamnosys-visicast}
	\end{figure}


\subsection{SignWriting} \label{sec:signwriting2}

SignWriting is a very popular system that uses highly iconic symbols, similarly to HamNoSys, but was designed for regular usage and not for phonetic transcriptions.
This system was designed by a dance teacher, with movement and expressiveness in mind.
In Brazil's education system it is part of the educational system.

SignWriting shares the problem of HamNoSys in that one gesture may be represented with different `spellings'.

\subsection{Movement-Hold} \label{sec:liddell}
%\cite{liddell03}
Liddell and Johnson published a serious of papers for the definition of their `Movement-Hold' model, with a first proposal in 1984 and a significantly revised version in 1989 \cite{liddell89}.

This model, depicted in Fig. \ref{fig:mov-hold}, hypothesizes that gestures are defined by a stating and end position - the holds - and by the movement in between. Movements and holds are defined as the `segments' of the gesture, separated with timing units from features that include non manual signs.

	\begin{figure}[h]
		\centering \includegraphics[width=0.4\textwidth]{sl/movement-hold}
		\caption{Movement-Hold model from Liddell and Johnson}
		\label{fig:mov-hold}
	\end{figure}

These authors argue that sign languages have articulatory sequentiality, that is, that signs have a sequential sub-lexical structure, and also explore phonological processes such as movement epenthesis and hold deletion (illustrated in Fig. \ref{fig:bellugi}). %, metathesis, gemination, assimilation, reduction, preservation and anticipation.

%showed that more phonological detailed models help identify morpheme structure and distinguish new patterns and result in new discoveries


The most recent work from the authors, from 2011, comprises a series of studies that exhaustively detail an evolution of their movement-hold model \cite{liddelljon1,liddelljon2,liddelljon3, liddelljon4}.

Figure \ref{fig:liddell11-chicago} shows an analysis that the authors did, frame by frame, to a recording of the gesture meaning `Chicago'. The sign begins and ends in rest position. The authors identify alignments in features as meaningful for the gesture and all the other changes a mere transition state.
From the meaningful alignments, feature matrices are extracted.

	\begin{figure}[h]
		\centering \includegraphics[width=0.9\textwidth]{papers/liddell11-chicago-edit}
		\caption
		[Analysis of the frames obtained in a recording of a gesture in ASL]
		{Analysis of the frames obtained in a recording of a gesture in ASL\\
		Key: `HC'--Hand Configuration, `PL'--Placement, `FA'--Facing, `NM'-Non-Manual}
		\label{fig:liddell11-chicago}
	\end{figure}

In relation to the previous model, postures and detentions are now distinguished and there is a more attention to timing and duration.

They state that, in their experience, this model should transfer well for sign languages other than the \ac{ASL}.





%SAMPA and systems to describe facial and non manual parts? phonetic

\begin{comment}
\textbf{Sign Representation for use in Computers}\\
SiGML and friends


way to describe sign language in a way that computers can process.

SWML (SignWriting Markup Language), an XML-based format, which has recently been developed for the storage, indexing and processing of SignWriting notation

\ac{SML} xml based to codify gestures and store description of signs in the database.
uses the joints defined in H|Anim with rotations defined with a duration and euler angle.

SiGML

\end{comment}


\subsection{Applied Notations} \label{sec:misc}

Several works develop their own systems for representation of sign language.
We provide as example a work focusing on sign language recognition \cite{paper97} that attempts to define gesture parameters for extraction from japanese sign language. %[5]

Figure \ref{fig:paper97} illustrates the model devised for this work, with the table of `chereme' decomposition on the left, and the exemplified gesture model on the right, using `C' (Concurrent) and `S' (Sequential) elements.


	\begin{figure}[h]
		\centering
			\begin{subfigure}[b]{0.5\textwidth}
					\centering\includegraphics[width=\textwidth]{papers/paper97_cheremes-table}
					\caption{Table of chereme decomposition}
			\end{subfigure}
			\quad
			\begin{subfigure}[b]{0.45\textwidth}
				\begin{subfigure}[b]{\textwidth}
					\centering\includegraphics[width=\textwidth]{papers/paper97_model-for-gesture}
					\caption{Model for gesture definition}
				\end{subfigure}

				\begin{subfigure}[b]{\textwidth}
					\centering\includegraphics[width=\textwidth]{papers/paper97_model-for-gesture-eg}
					\caption{Example usage}
				\end{subfigure}
			\end{subfigure}
			\caption{Sign Language decomposition model proposed by \cite{paper97}}
		\label{fig:paper97}
	\end{figure}


