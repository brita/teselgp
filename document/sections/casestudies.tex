
\section{Case Studies} \label{sec:case_studies}
Parallel to the development of the prototype, we devised a series of case studies to test the implementation and the flexibility of the architecture and technology choices.

We started with posing base hand configurations in a limited context case, passing then to full words, their derivations and blending between them. Finally, we tested the prototype with full sentences.


\subsection{Numbers} \label{sec:cs_numbers}
The first ``simple'' concept test that we devised, was the ``Numbers'', as it is a limited case that even corresponds to hand configurations. In a few minutes, however, it was obvious that the numbers are not a simple case at all.

The posing of the hand configurations alone, took several iterations of the rig, animation method and way to store the action library. The problems were due to this being the first test, not due to the test itself. Doing a set of base hand configurations to start, proved to be a good choice as it allows to test the hand rig and basic methodology.
The ten (0 to 9) hand configurations are shown in figure \ref{fig:num-configs}

\begin{figure}[h]
	\centering
		\begin{subfigure}[b]{0.17\textwidth}
				\centering\includegraphics[width=\textwidth]{fem/0}
		\end{subfigure}
		\hfill
		\begin{subfigure}[b]{0.17\textwidth}
				\centering\includegraphics[width=\textwidth]{fem/1}
		\end{subfigure}
		\hfill
		\begin{subfigure}[b]{0.17\textwidth}
				\centering\includegraphics[width=\textwidth]{fem/2}
		\end{subfigure}
		\hfill
		\begin{subfigure}[b]{0.17\textwidth}
				\centering\includegraphics[width=\textwidth]{fem/3}
		\end{subfigure}
		\hfill
		\begin{subfigure}[b]{0.17\textwidth}
				\centering\includegraphics[width=\textwidth]{fem/4}
		\end{subfigure}
		%newline
		\par\medskip\medskip\medskip\medskip
		\begin{subfigure}[b]{0.17\textwidth}
				\centering\includegraphics[width=\textwidth]{fem/5}
		\end{subfigure}
		\hfill
		\begin{subfigure}[b]{0.17\textwidth}
				\centering\includegraphics[width=\textwidth]{fem/6}
		\end{subfigure}
		\hfill
		\begin{subfigure}[b]{0.17\textwidth}
				\centering\includegraphics[width=\textwidth]{fem/7}
		\end{subfigure}
		\hfill
		\begin{subfigure}[b]{0.17\textwidth}
				\centering\includegraphics[width=\textwidth]{fem/8}
		\end{subfigure}
		\hfill
		\begin{subfigure}[b]{0.17\textwidth}
				\centering\includegraphics[width=\textwidth]{fem/9}
		\end{subfigure}
		\caption{Hand configurations in \ac{LGP} for numbers from 0 to 9}
	\label{fig:num-configs}
\end{figure}


As usual with animation, we looked for other references of the gestures in video form, besides the static images of hand configurations that can be found in the \ac{LGP} dictionary \cite{diclgp}.
We quickly found different interpreters, not only varying a lot in the signing, but also using completely different gestures.
This test also served as a rash introduction to the lack of resources to learn the Portuguese Sign Language and to the variability and regionalism that it is subject to.

More research proved that the correct sign for a number varies according to the context. Numbers can be used as a quantitative qualifier, as the isolated number (cardinal), as an ordinal number and as a number that is composed of others (eg. 147).

We found multiple forms of expressing \emph{um} (`one') and \emph{dois} (`two'), for example \emph{duplo} and  \emph{dobro} (as if for `twice' and `double'). There are also several forms of expressing quantity, repetition or duration as an adjective or complement to a noun or verb.

Reducing the test case to ordinal numbers, there is still the topic of how to express numbers in the order of the tens and up. Most cases seem to be ``fingerspelt'', for example, `147' is signed as `1', followed by `4' and `7' with a slight offset in space as the number grows. Numbers from `11' to `19' can be signed with a blinking movement of the units' number. Some numbers, in addition to these system, have a totally different gesture as an abbreviation, as is the example of the number `11'. 


\subsection{Rabbit} \label{sec:cs_coelho}
After isolated configurations, a couple of words were chosen, such as \emph{coelho} (`rabbit') with no serious criteria.

The word `rabbit' showcases several interesting topics, the first being that, while the gesture starts with a standard `U' hand shape, the fingers should then move alternately, demonstrating that a simple animation synthesis approach with blending between hand configuration, orientation and position is not enough. %more fine grained methods are needed, for the individual finger movement, contacts.. etc

Secondly, several words deriving from the Portuguese stem \emph{coelho} were implemented, such as \emph{coelha} (a female rabbit), \emph{coelhinho} (a little rabbit) and \emph{coelheira} (a rabbit house).

Contrary to the Portuguese language, where the stem is modified or added with a new morpheme, the gesture is not modified with a new phoneme, but followed with the full adjective, just as in english where there is no `one word' that bears the same meaning. The Portuguese sentences \emph{coelho pequeno} and \emph{coelhinho} would result in the same translation in \ac{LGP}, shown in Figure \ref{fig:catarina-coelhinho}.

\begin{figure}[h]
	\centering
		\begin{subfigure}[b]{0.2\textwidth}
				\centering\includegraphics[width=\textwidth]{fem/COELHO}
				\caption{\emph{coelho} / `rabbit'}
		\end{subfigure}
		\quad\quad
		\begin{subfigure}[b]{0.2\textwidth}
				\centering\includegraphics[width=\textwidth]{fem/PEQUENO}
				\caption{\emph{pequeno} / `little'}
		\end{subfigure}
		\caption{Generated gestures for the Portuguese word \emph{coelhinho}, meaning `little rabbit'}
	\label{fig:catarina-coelhinho}
\end{figure}

Adjectives, as a rule, follow the noun, but not always, and not in the case of the `female/woman' qualifier. Figure \ref{fig:catarina-coelha} illustrates the correct sequence for the word \emph{coelha}.

\begin{figure}[h]
	\centering
		\begin{subfigure}[b]{0.2\textwidth}
				\centering\includegraphics[width=\textwidth]{fem/MULHER}
				\caption{\emph{mulher} / `female'}
		\end{subfigure}
		\quad\quad
		\begin{subfigure}[b]{0.2\textwidth}
				\centering\includegraphics[width=\textwidth]{fem/COELHO}
				\caption{\emph{coelho} / `rabbit'}
		\end{subfigure}
		\caption{Generated gestures for the Portuguese word \emph{coelha}, meaning `female rabbit'}
	\label{fig:catarina-coelha}
\end{figure}

The plural, in Portuguese, is also expressed by modifying the stem, in the most general case, by adding an `-s'. However, plural cases are much more complex in \ac{LGP}, 
depending on the type of plural and on the word as described in \refwra{sec:lgp_plural}. There is not enough information or data to establish rules on this matter, and therefore, plurals were not explored.

As with all rules, there are exceptions, and thus the stemming system could not be generalized. The `female/woman' qualifier is often replaced by a specific different word, the plural forms are very dependent on the context, and even the adjective for expressing `small' is highly iconic. As explained by an interpreter, the gesture for `small' can be made in different directions, indicating wideness or tallness, and with different dimensions according to the case. As an example, the adjective for a rabbit would be signed with the actual size, while a `small elephant' can be signed with the hands far apart and still express smallness. For a giraffe, the adjective would probably be signed as small in height and not in width. The expressiveness of the gesture also indicates exactly how small the object is, matching varying degrees that are expressed by different words in Portuguese: \emph{pequeno}, \emph{pequenino}, \emph{pequeniníssimo}\dots




\subsection{``O João come a sopa''} \label{sec:cs_joao}
After testing isolated words, we proceeded a full sentence.
The chosen sentence was ``O João come a sopa'', roughly meaning ``John eats the soup'', being a sentence often used as a simple case for Portuguese Natural Language study.

The first big difficulty was the complete lack of online teaching resources or physical references that show how to utter something similar in \ac{LGP}.
Despite the existence of a few \ac{LGP} dictionaries and sign lexicons, there is only one, out of print, grammar. Without the personal help of the \acl{APS} we would simply not have been able to know how to translate the sentence.
With the help of an \ac{LGP} interpreter and the generous lending of the association's only copy of the grammar \cite{gramatica94}, we were able to translate the sentence and extract most of the interesting information that is present in Section \ref{sec:bg_lgp} on \ac{LGP}.

The classification of the Portuguese Sign Language is \acf{OSV}, while spoken Portuguese is predominantly \acf{SVO}. This means that the sentence's elements have to be reordered as depicted in Figure \ref{fig:casestudies-soup-alignment}. The conversion from Portuguese to \ac{LGP} is in gloss form.

\begin{figure}[h]
	\centering \includegraphics[scale=.95]{translation/sopa-joao-comer}
	\caption{Alignment for Portuguese and \ac{LGP} of the sentence meaning ``John eats the soup''}
	\label{fig:casestudies-soup-alignment}
\end{figure}

Being a rule, it has, of course, exceptions both for Portuguese and \ac{LGP}. The sign language, in particular, often does not respect the \ac{OSV} ordering, because it is not so established and formally taught.

The next difficulty was the use of a proper name, \emph{João}, that has to be fingerspelt. The appropriate way to fingerspell \emph{João} is either by the use of only the first three letters, or of the entire name as it is a short one. Accentuations on the name are mostly irrelevant for the distinction of Portuguese names, and therefore the hand configuration used is a normal `A'.

An interesting topic arose when discussing proper names, which is the use of \emph{gestural names}. It is normal among the deaf to name the children with a sign, often with a meaning in accordance to the person's characteristics. The use of gestural names, makes it manageable to reference entities quickly and unequivocally. Even in books for deaf children, characters and their gestural names are usually introduced at the beginning and therefore, the chosen sentence would be unlikely in a normal deaf conversation. However, in other situations, such has live interpreting in the news, this would be a valid use. It is important to pay attention to the context in which the system is used so as to treat proper names accordingly.

Lastly, the translation of the verb follows. In this case the use of the verb is plain, with no past or future participles, resulting in the use in the infinitive form in \ac{LGP}. However, the verb `to eat' in \ac{LGP} is highly contextualized with what is being eaten. The verb is signed recurring to different hand configurations and expressiveness, describing \emph{how} the thing is being eaten. This can be thought of as a verb agreement with the object in terms of grammatical aspect being also necessary the use of a classifier on how to handle the object. 

For the regular use of the verb, the hand goes twice to the mouth, closing from a relaxed form, with palm up. For eating soup, the gesture is done as if handling a spoon, for eating apples, the verb is signed as if holding the fruit, also, for eating an hamburger, the gesture is done as if holding it, indicating even the size of the hamburger. These contextualizations are not evident in the most recent and complete \ac{LGP} dictionary \cite{diclgp}.

The resulting animation is shown in Figure \ref{fig:joao-come-sopa}, broken down into its main gestures.

\begin{figure}[h]
	\centering
		\begin{subfigure}[b]{0.15\textwidth}
				\centering\includegraphics[width=\textwidth]{fem/SOPA}
				\caption{\emph{sopa} / `soup'}
		\end{subfigure}
		\hfill
		\begin{subfigure}[b]{0.15\textwidth}
				\centering\includegraphics[width=\textwidth]{fem/J}
				\caption{J}
		\end{subfigure}
		\hfill
		\begin{subfigure}[b]{0.15\textwidth}
				\centering\includegraphics[width=\textwidth]{fem/O}
				\caption{O}
		\end{subfigure}
		\hfill
		\begin{subfigure}[b]{0.15\textwidth}
				\centering\includegraphics[width=\textwidth]{fem/A}
				\caption{A}
		\end{subfigure}
		\hfill
		\begin{subfigure}[b]{0.15\textwidth}
				\centering\includegraphics[width=\textwidth]{fem/O}
				\caption{O}
		\end{subfigure}
		\hfill
		\begin{subfigure}[b]{0.15\textwidth}
				\centering\includegraphics[width=\textwidth]{fem/COMER_SOPA}
				\caption{\emph{comer} / `eat'}
		\end{subfigure}
		\caption{Sequence of generated gestures corresponding to the Portuguese sentence ``\emph{O João come a sopa.}''}
	\label{fig:joao-come-sopa}
\end{figure}

This is a perfect example on how Sign Language can be more expressive and work with more information than Portuguese.
The system needs to know not only what is the subject and verb, but also to establish a semantic relationship to represent that the object - the soup - is being eaten - the action. Finally, the system needs world knowledge to know that soup is handled with a spoon to finally produce the correct action in the correct concordance.



%\subsection*{Adding Vocabulary to the System}
