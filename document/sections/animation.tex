
\section{Animation} \label{sec:anim}

Animation (from Latin anim\={a}ti\={o}, ``the act of bringing to life'') is defined as a series of images that, when played in sequence, give the illusion of motion.
Animation exists as an art and communication form since the prehistoric period, and has met significant technical innovation during the 19th century, with the development of tools capable of rapidly displaying sequences of drawn images (like the \emph{zoetrope}). At the end of the 19th century, the advent of motion picture pushed animation further, leading to the birth of traditional animation.

Animation as a sequence of drawings had its early beginning in 1896 and established its basis in the following years until, inclusive, the so called `Golden Age' of Animation in the 30s-40s. This age featured American giants such as \emph{Disney}, \emph{MGM} and \emph{Warner Bros} and their most successful classics. The use and experimentation of innovative techniques, color and sound in these works evolved the art and craft of animation.

This success declined in the 50s due to the rising of Television and World War II to be retaken later. Progress in animation became mostly a matter of technology, with better equipment and the emerging of computing, followed by computer animation and early 3D in the 80s. Toy Story marked a turning point in 1995, being the first feature-length computed-animation film and also Pixar's first production.

The foundations of animation as `drawings in time' were invented and refined in Hollywood's `Golden Age' and most concepts are still relevant today, independently of the technology and medium used. Both classical and computer animation need to solve the problems of how to provide movement, weight, time and empathy.

This section describes important animation concepts that are relevant for this work. Some of these concepts come from classical animation and others are specific to computer generated images and the technology that makes 3D computer animation possible. The section completes with an overview of techniques used to convey believability to animation, taking characters into consideration.

\subsection{Classical Animation} \label{sec:classAnim}
Classical, or `traditional', animation is a broad topic that influences digital animation and works such as this, that aim to synthesize animation that is realistic and believable.

We list the main foundational principles for doing so, rooting from classical animation and reference the reader to broader books on the subject \cite{animSK,illusionoflife}


\subsubsection{The 12 principles of animation} \label{sec:12p}
In 1981, Walt Disney animators Ollie Johnston and Frank Thomas described a set of animation principles in their foundational book ``The Illusion of Life'' \cite{illusionoflife}.
These principles were distilled out of decades of experience in animated film production and define guidelines to create believable and appealing animation.
Such guidelines and principles are still up-to-date and are used both in digital and traditional animation.

Here follows a brief explanation of each principle, with illustrations in \refwraa{app:12p}.

\paragraph*{Squash and stretch}
This first and fundamental principle is meant to give a sense of weight to the animated object. The principle can be applied to both rigid or flexible objects. Squash and stretch is often exaggerated in order to obtain a comical effect.

\paragraph*{Anticipation}
Anticipation consists in an action preparing for the main action in a shot. Besides being a natural phenomenon, anticipation helps the viewer to foresee what is about to happen on screen. For example, if a character is about to jump, she will bend her legs to actually have the energy to do the jump.

\paragraph*{Staging}
This principle highlights the importance of the presentation of an idea, a character or an action in a way that can be easily and clearly understood by the audience. This usually translates in the proper positioning of the character in the frame of a shot and in the use of effective lighting.

\paragraph*{Straight ahead action and pose to pose}
More than a principle, straight ahead and pose to pose are two different methods for drawing the frames of a shot. In the first case, the drawings are created in sequence, allowing the artist to have more control on the flow of the action, but less control on the overall changes of shape due to the deformation of the character. In the second case, the animator defines a number of key poses along the duration of the animation and later on fills in the missing frames.

The later is often used in computer animation, since animation software can partially assist in calculating the missing inbetween frames.

\paragraph*{Follow through and overlapping action}
This principle is meant to describe the inertia of loosely tied parts (like an antenna, a tail, a limb) of a moving body that comes to a stop. Like squash and stretch, it can help to convey size and mass of a character or an object. 

\paragraph*{Ease in and Ease Out}
Natural movement is characterized by acceleration at the beginning of an action and deceleration at the end. By distributing unevenly the inbetween frames of an action, it is easily possible to achieve this principle.

\paragraph*{Arcs}
Most natural action tends to follow an arched trajectory, and animation should adhere to this principle by following implied ``arcs'' to achieve greater realism. This can apply to a limb moving by rotating a joint, or a thrown object moving along a parabolic trajectory. The exception is mechanical movement, which typically moves in straight lines.

\paragraph*{Secondary action}
This principle refers to the multitude of secondary events during the main animation of a body. Such events are meant to recreate a believable complexity and reinforce the main action of the shot.

\paragraph*{Timing}
Timing is used in relation to other principles, to define the weight of an object, the staging of a character and so on. This is one of the most important principles affecting the believability and readability of an action.

\paragraph*{Exaggeration}
Exaggeration is a technique used to stylize and emphasize specific aspects of an action, in order to better communicate the mood of a shot, as well as make other principles more visible.

\paragraph*{Solid drawing}
This principle is particularly valid for traditional, hand-drawn animation, and stresses the need of good draftsmanship in order to represent correctly and consistently animated bodies or objects.

\paragraph*{Appeal}
Appeal is the overall exterior quality of a character, that makes the audience capable of relating to it. Playing with symmetry, good proportions and clean posing can greatly increase the appeal of a character.

\subsection{Computer Animation} \label{sec:compAnim}
Computer animation shares most of its core concepts with traditional animation (especially regarding the principles), but offers the artist a completely different set of tools and techniques to work with.

In the following paragraphs we describe the relevant workflows and concepts that will be later used in this work in order to create an animated 3D character, reproducing Portuguese Sign Language utterances.

\vspace{0.3cm}

One of the core differences between traditional animation and computer animation is the way that frames are generated. In the first case, the animator has to draw each frame by hand on paper, while in the second case, a 3D model has to be properly \emph{posed} in each frame on a digital screen.

The digital posing of a model is possible due to a dedicated interface built into the 3D model of the character. This interface and its controls are called a \emph{rig}. A rig is composed of \emph{bones}, which are often organized in hierarchies, and further controlled using constraints, to limit or automate their behavior. For example, a `copy rotation' constraint allows a bone to copy the same rotation value from another bone. One other important constraint for character animation is the \ac{IK}, which will be mentioned more in depth later on.

The interaction between a 3D model and a rig is made possible by a process called \emph{skinning}, where the geometry of the model is associated with the components of the rig. This process works by assigning an influence from a bone to each vertex of the mesh that will, from then on,
% move along with the bone according to said influence.
have its location affected by that of the bone according to said influence.
The result of this process allows an animator to pose the rig of a character and have its mesh (the visible shapes) to follow in real-time.

\vspace{0.3cm}

In order to create an animation, it is necessary to record a series of poses of the character that will be then played back in sequence. This is done by using an interface called \emph{timeline}, which displays all the frames that are part of the animation, and allows the artist to \emph{scrub} along, play them forward or backwards, and so on.
To record a pose, the animator selects the desired frame, poses the character accordingly and then saves the pose. What actually gets saved are the rotation, translation and scale values of each component of the rig.
Frames containing saved poses are called \emph{keyframes} and the process of saving is named \emph{keying}.

\emph{Retiming} is possible using a dedicated editor, called \emph{dopesheet} or \emph{x-sheet}, which is similar to the timeline, but allows the selection and translation of keyframes.

\vspace{0.3cm}

The major difference between traditional and computer animation, as already mentioned, is that in the case of the latter, the frames between keyframes, the \emph{inbetweens}, are automatically interpolated by the computer. This process can be visualized and controlled in another dedicated editor for \emph{curves}.

The curve editor displays functions with time in the $x$ coordinates the keyed value in $y$ axis. The keyed values (location, orientation and scale) each have components such as $x$, $y$ and $z$. These are called the \emph{channels} of the animation.

The interpolation between the values can be controlled for each individual keyframe using dedicated handles. Interpolation can be based on b-spline, stepped or linear curves. Each type fits different styles and stages in the animation process.

\vspace{0.3cm}

What has been described thus far, regards the creation of a single action. In order to create complex action and keep parts of the animation reusable, it is possible to use an editor that allows the combination of multiple actions (for example, a walk cycle - which will get looped - and a speaking action that is mostly facial animation). This way it is possible to layer actions and have great control over the final result. 

\vspace{0.3cm}

An alternative method to manual keying, is the use of \emph{motion capture} technology to acquire motion information from a real life source.
This technique resorts to a combination of highly specialized video capture tools and custom wearables and markers, although research is very active on the use of unobtrusive and inexpensive equipment and, in the limit, of a regular camera by applying software markers.
The process of resolving footage by applying digital markers is called \emph{motion tracking}.

While this technique can provide very realistic results rather quickly, it is limited by the quality of the information acquisition tools and on the type of performance to be recorded.


\subsubsection{Forward and Inverse Kinematics}
	\ac{IK} is a type of constraint and animation technique that works by posing the \emph{end effector} of a \emph{chain} and having the intermediary segments being calculated automatically. \ac{FK} is the inverse process, where the chain segments' transforms are calculated in a sequential way, or `forward'.
	
	Given a \emph{chain of bones}, with a root and a tip, how a segment, when moved, affects the others before and after itself, is given by the calculation of Inverse or Forward Kinematics. The root of the chain can also be called \emph{base}, and the tip is an \emph{end effector}.
	
	When using \ac{FK}, the transform of a bone affects hierarchically the transforms of all those beneath it in the chain and has no influence on the ones before it. This method is computationally very cheap and also intuitive for the animator. It is not, however, the best methodology for all tasks, including posing a character's arms to gesture sign language.
	
	From the animator's perspective, \ac{FK} requires to pose first the shoulder, then the upper arm, the forearm, the hand, and finally, all of the segments of the fingers in order. The technique is employed in scenarios with loose arm swings, being \ac{IK} preferred for the manual sign language scenario, where gestures are precise and contained in strength. \ac{IK} is practically mandatory in situations of contacts and grasping.
	
	\ac{IK}, from the animator's perspective, works by posing the end-effectors directly in the desired final position, for example placing the hand in the chest, allowing the system to calculate the position for the forearm, upper arm and shoulder. It is customary to also define the direction of the elbow joint.

	When the hand (the end effector) is moved, the bones before it in the chain follow it, without breaking the chain and keeping the base in place. The segments' position and orientation are calculated by the \ac{IK} \emph{solver} that works with a set of constraints such as not breaking the chain and the joints' \ac{DOF}.
	
	A joint's \acf{DOF} are relative to its rotation (although there are also \emph{Translational Joints}, they do not apply to a human body). A joint is said to be an \emph{Hinge Joint} if having 1 \ac{DOF} (examples: elbow and fingertips), an \emph{Universal Joint} with 2 \ac{DOF} (example: finger knots) and a \emph{Free or Gimbal Joint} with 3 \ac{DOF} (example: neck).
	The \acp{DOF} of human joints are hard to assess, being a very complex bone and muscular system. Often joints do have the degree of freedom, but in a limited angle or interdependent way.

	The \ac{IK} solver is an iterative algorithm to satisfy constraints while attempting to reach a the end effector's target location. It is typically modeled as a Jacobian Matrix. The following references provide good reading material on Jacobians, transposes and inverses for \ac{IK}\cite{buss2004introduction,	buss2005selectively, boulic1996hierarchical, boulic1995inverse,	baerlochthesis}
	  

\subsection{Relevant software and technologies} \label{sec:technicalities}
The computer graphics field exists since a few decades, during which we have assisted to development of a very wide landscape of related software tools. A large number of these tools exist in relation to the film and video game industry and consists mainly of in-house or proprietary software. 

The last decade has shown a change of this trend, with several major motion picture studios open sourcing some of their technologies, in order to allow easier data exchange between applications and departments, accelerate research and mainly try to establish such technologies in the market with a dominant position.

It is currently possible to create complex and rich visuals entirely with free and open source software, some of which will be mentioned shortly.

The reasons why such a variety of tools exists, can be found in the workflow adopted to produce computer animation. The number of tasks in an animation pipeline is so high and specific, that many solutions are possible and often depend on artistic and technical choices.

Two main workflows are possible: one is to have an integrated pipeline, with one software package acting as the sole working platform, another is to fragment the different tasks along several packages and have an input/output pipeline connecting them. The choice of workflow depends on several factors, such as the complexity of the production, the deliverables, and so on.

\paragraph{Blender}\scite{blender}
Blender is a free and open source 3D animation suite, offering a complete toolset to produce advanced computer animation, in a professional production environment. More than a decade of use across several film and game studios has made Blender a reliable and powerful tool.
With Blender it is possible to create and maintain a self-enclosed pipeline (relying completely on Blender itself), as well as integrate it with other tools, thanks to a solid data import/output toolset.
In most cases it is very convenient to just use Blender, since it can cover modeling, shading, animation, rendering and video editing. In this work, we are mainly going to leverage on the rigging and animation features, employing some advanced functionality such as the \ac{NLA} to combine and layer multiple actions of a 3D avatar for high levels of complexity and realism.

\paragraph{MakeHuman}\scite{makehuman}
MakeHuman is an Open Source tool for making 3D human characters. The tool was first released in 2001 and is well established.

The MakeHuman team has done extensive research on topology throughout the years and the models are well proven for animation and real time use.

MakeHuman functions based on linearly interpolating a set of parameters to configure the mesh. The parameters, such as gender and age, are presented as sets of sliders. This is done by defining a target mesh, called a \emph{morph target}, as a parameter with full weight, that is then linearly mixed with the current mesh according to the value in the corresponding slider. There are over 1170 morphing targets.

This approach is called \emph{parametric modeling} and has the advantages of being a fairly quick methodology that requires no knowledge and expertise on 3D modeling.
The tool has virtually no learning curve. It is possible to setup an avatar in less than five minutes by just dragging sliders and clicking export.

MakeHuman is written solely in Python in a pure plugin architecture, making it very portable and compatible with other software packages.
MakeHuman provides a set of addons for clothing and motion capture for use in Blender as well as a good exporting system that includes Blender's main human rigging tool - Rigify.

\paragraph{H|Anim} \label{sec:hanim}
H|Anim is an approved ISO standard for humanoid modeling and animation proposed by the Humanoid Animation Working Group \scite{hanim}

The specification defines a series of mandatory joints, end-effector hierarchies for \ac{IK} chains, and other rules and properties for interchangeable human figures that can be used across a variety of 3D games, simulation environments and \ac{VRML} supporting browsers.

The working group and ISO standard was created in the 90s, and although there is still some activity, the design and technology used is now obsolete for today's standards.

The official page seems unmaintained and outdated, with most resource links broken. Most of the information was gathered through other channels, such as the web3d consortium page \scite{x3d-hanim}.

A standard implementation would be very limited and performance hindered by the underlying technology. Identified problems are the skinning and IK methods, use of \ac{VRML} and the verbose text file format.

This technology is also very limiting and cumbersome to use, because it is not widely supported and there are many limitations in the character modeling, rigging and posing, rendering most resources unusable.

