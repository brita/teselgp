\section{Implementation} \label{sec:imp}
We implemented a prototype to serve as a framework or proof of concept.
In this section we describe our implementation and justify our choices.



	\subsection{Blender} \label{sec:imp_blender}
	We chose Blender as the 3D package to use for modeling and animation. Blender is a very powerful tool used in the animation and games industry as well as for research and academic projects.
	
	The main reason for choosing Blender is that it is an open-source project with an also open community of developers and artists.
	We have free access to the software, the code, documentation and to the expertise of both developers and artists.
	
	Blender has a powerful and flexible API paradigm, that allows accessing and operating on all the data (animation, mesh, shots \dots) via scripting, that we use in this implementation. The same philosophy applies to scripting access to the interface, that we also exploit.
	Blender has tools for the modeling of characters and for importing other mesh formats, it has a great focus on animation tools and also has facilities for shot editing and video exporting.
	

	\subsection{Addon Philosophy} \label{sec:imp_addon}
	The choice of Blender as a modeling and animation tool implies some others, such as how to interface our system with the 3D model and the animation data and how to display the animation.
	
	Blender is designed in a way that facilitates users to design scripts for multiple purposes, such as automating tasks and building new tools.
	The scripts can interact with the internal data structures, operators on said data, and with the interface. With little modifications, the scripts can be provided as addons for ease of sharing with other users.

	We saw no reason to deviate from this design, as our needs for displaying the animation and for interfacing with the code and the user are satisfied, although somewhat limited.

	At this moment, Blender has a very flexible interface, allowing the adding of panels with buttons in a very configurable way. However, it does not support the construction of entirely new widgets or editors and does not allow stripping down non necessary components for simplification. The implemented interface is described in \refwra{sec:imp_ui}.

	Blender only provides hooks for scripting in Python, that is, therefore, the only choice for the coding language to implement the system. We do not see a limitation in Python or on the possibilities to handle Blender's internal data (the animations).
	%It is possible, as a different approach, to run Blender in background mode with a thin Python script that extracts the animation data to input in a standalone system.
	
	Further considerations on how to display the avatar are explained in \refwra{sec:imp_playback}.


	
	\subsection{\acl{NLP} and Translation} \label{sec:imp_nlp}

	Following the choice of Python for implementing all of the system's logic, we chose to use the \ac{NLTK}\wcite{nltk} for \ac{NLP} tasks. \ac{NLTK} is a Python toolkit for natural language processing, providing many utilities and free to use corpora. Among the utilities are tokenizers, POS-taggers, stemmers, dependency parsers and \ac{NER}.
	
	Figure \ref{fig:imp_nlp} shows an overview of the implemented pipeline.

		\begin{figure}[h]
			\centering \includegraphics[width=\textwidth]{system/imp-nlp}
			\caption{Natural Language Processing pipeline used in the implementation}
			\label{fig:imp_nlp}
		\end{figure}
	
	\begin{itemize}
	\item \textbf{Error correcting and normalization} We implement a very basic step that enforces lowercase and the use of latin characters. Common spelling mistakes in the words used for the test cases are corrected case to case (eg. \emph{soupa} instead of \emph{sopa} or \emph{Joao} instead of \emph{João}).

	\item \textbf{Tokenizer} The input string is split into sentences and then into words. The tokenizer, provided by \ac{NLTK}, uses Portuguese language training.
	Example of a tokenized input from the use case \refwra{sec:cs_joao}:
	\verb|['o', 'joão', 'come', 'a', 'sopa']|

	\item \textbf{POS-Tagger} For POS-tagging, we use \ac{NLTK}'s n-gram taggers. Figure \ref{fig:n-gram-tagger} illustrates the principle behind this method, of considering the most used classification for a given word given the context of the $n-1$ tags applied before it.
	
	In our implementation, we start with a bigram tagger, with a backoff technique for an unigram tager and the default classification of `noun' as it is the most common class for Portuguese. There were not previously trained taggers for portuguese and, therefore, we used the suggested\cite{nltkbook} treebank `floresta sintá(c)tica' corpus \cite{floresta} for training.

		\begin{figure}[h]
			\centering \includegraphics[width=.5\textwidth]{system/tag-context}
			\caption{n-gram POS-Tagger illustration, from \cite{nltkbook}}
			\label{fig:n-gram-tagger}
		\end{figure}
		
	In the same example as above, the result would be:\\ \verb|[('o', 'art'), ('joão', 'prop'), ('come', 'v-fin'), ('a', 'prp'), ('sopa', 'n')]|

	\item \textbf{Named Entity Extraction} We apply \ac{NER} only for proper names of persons by matching against a list of common Portuguese names. Our system further supports a list of public personalities, such as the current Portuguese prime minister, with their matching gestural name. For these specific recognized entities, the system uses the known gesture instead of fingerspelling the name.

	We have made experiments with the AlchemyAPI\footnote{\url{http://www.alchemyapi.com}} that supports ``competitive'' \ac{NER} and Sentiment Analysis for European Portuguese. However, this technology was quickly rejected. AlchemyAPI has a free license and Python bindings, but all the algorithms and data are closed, the results were very poor for \ac{NER} and Sentiment Analysis only works for english at the moment.
	
	\item \textbf{Stemmer} As a form of morphologic parsing, we apply a stemmer that identifies suffixes and prefixes to use as an adjective or classifier to the gloss. The result is treated as a whole. For example, the portuguese word \emph{coelhinha}, meaning roughly `little female rabbit', is understood, by its suffix, to be a female and small derivation of the root \emph{coelh(o)}. Therefore, we convert this to \verb|MULHER, COELHO, PEQUENO|, hinted to be all part of the same gloss.
	
	\item \textbf{Lexical Transfer} We convert all the remaining and expanded words to their corresponding gloss using the dictionary lookup. This works by referencing to the database described below, where the word conversions are stored, along with the matching actions for animation that are passed to the animation stage. We do not pass along unrecognized words, meaning that any word that is not in the database is ignored. We provide an option in the interface to fingerspell them instead. Examples are \verb|['GLOSS', ['SOPA']]| and \verb|['FINGERSPELL', ['J', 'O', 'A', 'O']]|.

	\item \textbf{Structure Transfer}
	The implementation only supports basic re-ordering of sequences of `noun - verb - noun', in an attempt to convert the \acs{SVO} ordering used in Portuguese to the more common structure of \acs{OSV} used in \acs{LGP}. In the same example,\\ \verb|[['GLOSS', ['SOPA']], ['FINGERSPELL', ['J', 'O', 'A', 'O']], ['GLOSS', ['COMER-SOPA']]]|
	
	The other basic type of reordering is the switching of adjectives and quantities to the end of the affected noun.  As an example the input \emph{dois coelhos}, meaning `two rabbits', would result in \verb|[['GLOSS', ['COELHO']], ['NUMERAL', ['2']]|.
	\end{itemize}



We use the information from the POS-tag categories and NER to build hints for the animation stage. 
Currently recognized hints by the animation system are a normal word/gloss \verb|GLOSS| composed of one or more units, words that are fingerspelt with \verb|FINGERSPELL|, and numeric quantities, such as in \verb|['NUMERAL', ['2']]|.



\subsection{Lookup} \label{sec:imp_bd}
The animation lookup, given a gloss, is done via a JSON file mimicking a database.
This was thought to be the simplest and more portable solution for the data, without the strong need to recur to more solid ways of persistence in a proof of concept stage.

JSON datatypes translate well into Python dictionaries and lists and can later be upgraded to a web-scale, document database such as MongoDB\wcite{mongodb}.

As to the design, the database consists of a set of \emph{glosses} and a set of \emph{actions}. The action ids are mapped to blender actions, that are in turn referenced by the glosses. One gloss may link to more than one action, that are assumed to be played sequentially. This design can be inspected in Figure \ref{fig:bd}, where it can be seen that \emph{coelho} (`rabbit') has a one-to-one mapping, that \emph{casa} (`house') corresponds to one action and that \emph{cidade} (`city') is a composed word, formed by \emph{casa} and followed by another morpheme with no isolated meaning that is signed with both hands in an inward in `B' configuration that move up.

	\begin{figure}[h]
		\centering \includegraphics[width=0.85\textwidth]{system/bd}
		\caption{Database design used in the implementation}
		\label{fig:bd}
	\end{figure}

Knowing that gestures in \ac{LGP} can be heavily contextualized and inflexed with both subject and object for the same meaning in European Portuguese,  we added to the gloss structure an array of contexts with associated actions as can be seen in Fig \ref{fig:bd_sopa}. The figure shows the case of the verb \emph{comer} (`to eat') that is classified with what is being eaten as further explored in \refwra{sec:cs_joao}. When no context is given by the \ac{NLP} module, the default is considered to be the sequence in `actions'.

	\begin{figure}[h]
		\centering \includegraphics[width=0.65\textwidth]{system/bd2}
		\caption{Database design supporting gloss contextualization}
		\label{fig:bd_sopa}
	\end{figure}

We do not know yet if this design escalates well as this hard to devise a good database architecture when the rules and corner cases of the language are not clearly defined.



\subsection{Animation} \label{sec:imp_anim}

	For the animation pipeline, we start by seting an avatar for use in the system by rigging and skinning.

	We used Blender's \emph{Rigify} tool for rigging the character. Illustrations of the rigged character are in \refwraa{app:add-imgs_imp}, Fig. \ref{fig:rig1} and Fig. \ref{fig:rig2}.
	Rigify already provides \ac{IK} chains for the arms, rooting in the spine.
	% It also provides a custom set of controls for curling the fingers.

	Blender's \ac{IK} solver\footnote{\url{http://wiki.blender.org/index.php/User:Brecht/SummerOfCode2005/Inverse_Kinematics}} supports joints with different \acp{DOF} and translational joints, tree structured chains with multiple end effectors, rotational limits for joints, joint pinning and configurable effector influence and stiffness for each joint.
	The solver uses a Jacobian Pesudo-Inverse method with a Damped Least Squares method for resolving singularity problems.

	We have tagged key locations with bones in the body and face of the avatars so as pose the gestures procedurally as in Fig. \ref{fig:spots}.

	All the 57 different hand configurations for \ac{LGP} were manually posed and keyed from reference images gathered from \cite{diclgp} and the Spread the Sign project. The poses can be seen in \refwraa{app:hand_configs}.
	
	The animation is synthesised by directly acessing and modifying the action and f-curve data.
	We always start and end with the rest pose and then concatenate

	For concatenating the actions, we \emph{blend} from one to the other in a fixed amount of frames by using Blender's \ac{NLA} tools that allow action layering.
	Channels that are not used in the next gesture, are blended with the rest pose instead.

	Figure \ref{fig:nla} illustrates the result for the gloss sentence `\verb|SOPA J-O-A-O COME|'.

		\begin{figure}[h]
			\centering \includegraphics[width=0.8\textwidth]{system/nla}
			\caption{Action layering resulting of a sentence translation}
			\label{fig:nla}
		\end{figure}
	
	We adjust this number of frames for blending according to the hints received. For fingerspelling mode, we expand the duration of the hand configuration (that is originally just one frame) and blend it with the next fingerspelling in less frames than when blending between normal gloss actions. We also expand this duration when entering and leaving the fingerspell.

	For the fingerspelling mode, we needed to directly access the fcurve data, as this can not be done with the NLA tools. A figure of a resulting curve for a gesture is present in \refwraa{app:add-imgs_imp}, Fig. \ref{fig:fcurves}.


\subsection{Avatar} \label{sec:imp_avatar}
	Our implementation allows a large degree of freedom in the choice of avatar. The only restrictions are that the character must be imported into Blender and skinned to a \emph{rigify} armature. It is assumed that the character is a humanoid with two hands with five digits each and a regular enough face and proportions.
	
	With the definition of key locations as explained in \refwra{sec:arch_rig}, avatar variability is supported.
	%The animation is tolerant to predictable character proportion variation.
	Throughout the development of the proof of concept, several characters were tested, as illustrated in Figure \ref{fig:avatars}. From the left to the right, there is \emph{Sintel} and \emph{Blenderella}, made freely available by the Blender Foundation as part of an open movie and training respectively, next a  downloaded model\footnote{\url{http://www.blendswap.com/blends/view/13464}} made by the community and freely licensed. In this document, we always present the rightmost character, \emph{Catarina}, acquired by the \ldf group.

	\begin{figure}[h]
		\centering
			\begin{subfigure}[b]{0.2\textwidth}
				\centering \includegraphics[width=\textwidth]{system/sintel2}
			\end{subfigure}
			\hfill
			\begin{subfigure}[b]{0.2\textwidth}
				\centering \includegraphics[width=\textwidth]{system/blenderella}
			\end{subfigure}
			\hfill
			\begin{subfigure}[b]{0.2\textwidth}
				\centering \includegraphics[width=\textwidth]{system/dude-crop}
			\end{subfigure}
			\hfill
			\begin{subfigure}[b]{0.2\textwidth}
				\centering \includegraphics[width=\textwidth]{system/J}
			\end{subfigure}
		\caption{Example of the range of avatars supported by the prototype}
		\label{fig:avatars}
	\end{figure}

	A workflow using MakeHuman to quickly produce realistic humanoids was also tested. Indeed, in about two minutes, we can have an highly customized human as shown in Figure \ref{fig:makehuman}. However, the same is not truth for the character's clothing. Though there are some freely available resources, it is required that the clothes be modeled in Blender.
	The character comes fully rigged with \emph{rigify} and skinned out of the box with an excellent topology, being the only problem, in fact, the lack of clothing.


	\begin{figure}[h]
		\centering
			\begin{subfigure}[b]{0.32\textwidth}
					\centering\includegraphics[width=\textwidth]{system/makehuman1}
			\end{subfigure}
			\quad
			\begin{subfigure}[b]{0.31\textwidth}
	  		\centering
					\begin{subfigure}[b]{\textwidth}
							\centering\includegraphics[width=\textwidth]{system/makehuman2}
					\end{subfigure}
					\par \medskip
					\begin{subfigure}[b]{\textwidth}
							\centering\includegraphics[width=\textwidth]{system/makehuman3}
					\end{subfigure}
			\end{subfigure}
		\caption{Out of the box skinned mesh with \emph{Rigify}, exported from MakeHuman}
		\label{fig:makehuman}
	\end{figure}
	



	\subsection{Animation Playback} \label{sec:imp_playback}
	How the animation, after generation, is played is up to the capabilities of Blender and the needs and use cases that an application using this technology may have.
	
	To view an animated avatar, it is necessary to render it. Blender supports different render engines aimed at film and still image production. These renderers or even \nth{3} party ones, strive for quality at the cost of render time, and are not suited to our needs.
	
	The renderer most tuned for speed in Blender is the often overlooked \emph{Viewport Renderer}, also called \emph{OpenGL Renderer}\footnote{Although the name suggests otherwise, all renderers in Blender use OpenGL and optionally GLSL technology.}, that is used to render the contents of the 3D View while they are being edited, in real-time.
	This renderer is also used by animators to render and share their work in progress, having no need for complex lighting and compositing.

	Blender also has its own builtin real-time engine called \ac{BGE}. The purpose of this feature fits exactly ours of having an interactive 3D application, however the \ac{BGE} is quite outdated and it is poorly supported. Its usage would imply an internal conversion of all the data structures and mechanisms such as the rig, that would result in several problems. In addition, it has no native support GUIs, that we need for user interaction.
	
	Finally, it is possible to export the avatar and animation to another engine, but this is always a lossy process that we did not deem needed.

	Considering all the options, we opted for the viewport playback mode as the interface possibilities are sufficient and there is no need for more advanced renderers. This makes it possible to develop the prototype in an addon philosophy, having everything integrated: the input, computations and output.

	A very important consideration is how to play the avatar in websites and mobile devices. While the support for this is currently not the best, it is possible to embed the animation in a webpage using Blend4Web\footnote{\url{http://www.blend4web.com/en/}} while progress is being made by the Blender developers to improve this situation.
	
	For more static content, Blender can be used to export a video of the animation. Blender has a video sequencer and editing capabilities that can be used by advanced users. For more casual users, we make available a minimal interface for video exporting. Using video, however can be a data intensive choice to store and download.
	


\subsection{User Interface} \label{sec:imp_ui}
The interface development took several iterations.
In its most basic form, it consists of an input text box, a button to translate, and a 3D view with the signing avatar. The 3D view can be rotated and zoomed, allowing to see the avatar from different perspectives.

The breakdown down in Fig. \ref{fig:ui} shows the main translation interface in blue, in `Interface Translation' and `Signing Avatar'.

	\begin{figure}[h]
		\centering \includegraphics[width=\textwidth]{system/interface}
		\caption{User Interface for the prototype}
		\label{fig:ui}
	\end{figure}

Additionally, we provide an interface for exporting video of the signing that supports choosing of the resolution, aspect ratio and file format. This panel is indicated in the image in orange.

Displayed in green, is a panel indicating the authors and describing the project.

All panels but the one used for the main interaction start folded.

It is still possible to use all the functionalities of Blender, thus making advanced usage of the system.



