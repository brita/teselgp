
%intro
As depicted in Fig. \ref{fig:mt_source-target} the aim of \ac{MT} is to convert from a given source language $f$ into a target language, denoted as $e$, that conveys the same meaning. The $f$ and $e$ notation for source and target sentences, respectively, is due to an early work on Statistical Machine Translation with the aim of converting French to English. This terminology is still used, along with the term '\emph{decoding}'.

\begin{figure}[h]
%	\centering \includesvg{images/translation/source-target}
	\centering \includegraphics[scale=0.8]{translation/source-target}
	\caption{Base scheme of Machine Translation from a Source to a Target Language}
	\label{fig:mt_source-target}
\end{figure}


%different approaches
There are several different approaches to \acf{MT}, but they can be divided in two major classes: \emph{rule-based} and \emph{data-based}.

The first class of algorithms defines a series of hand-made rules on how to convert from one language to the other. Expert linguists are required for the creation of these rules, and they often become very complex and increasingly troublesome to tune.

Translation data-based algorithms rely on statistics over very large bilingual aligned corpora.

More recent methods are hybrid, combining the advantages from both worlds, or using supervised machine learning as a way to derive the rules that were previously hand made by linguists.

%all approaches deal with LN problems
All of the approaches have to deal with ambiguity, linguistic variability and the common natural language problems mentioned above. Besides, when translating from one natural language to another, other problems arise due to the differences in vocabulary and grammar.

%annotation and use of more knowledge
The translation methods can be improved, or require that, the text to translate be annotated with syntax, context and relations that add to the system's knowledge and improve disambiguation quality. These annotations can be made by human linguists or automatically. Most relevant techniques are described in \refwra{sec:bg_ln-analysis}.

%why does this section even exist
An overview of existing methods is provided here, as the choice of what method to use for the pair European Portuguese and Sign Language Portuguese is not a given one.



\subsubsection{Rule-Based Machine Translation}
	%out of fashion but still in order for sls
	\ac{RBMT}, also called the `Classical Approach' of machine translation, has now mostly fallen out of flavor in favor of statistical methods and machine learning.
	However, as \acp{SL} suffer from a serious problem of lack of corpora, this methodology may be a good approach.
	
	This methodology relies on linguistic information on both the source and target languages to be able to convert one into the other. Indispensable such information are dictionaries and grammars.
	A dictionary is defined as an unilingual or bilingual collection of words, defining the lexicon of the language available for translation. The grammar is defined as a set of formally defined rules, as explained below.
	
	Inside \ac{RBMT} there are different approaches: \emph{dictionary-based} and \emph{transfer-based}. The first, dictionary-based translation, is mostly a word-to-word direct translation using a bilingual dictionary. It can also use a set of basic reordering rules and other small changes, but it is mostly a very simplistic approach, appropriate only for very close language pairs where the differences are mainly in vocabulary. The gap between European Portuguese and \ac{LGP} is very big and it is likely that no pair of spoken and signed languages can be translated in this way.
	
	A transfer-based approach is based on the concept of first `understanding' the input into an intermediate semantic representation and then encode it in the target language.
	
	To `understand' - or analyze - a language, a set of algorithms for morphological, syntactical and semantic analysis are employed, resulting in automatic annotations of the input. The annotations can range from word categorization (noun, verb, \dots), morpheme analysis, other context, dependencies and semantic roles. The input is then said to be parsed, having structures to support it, such as a tree.
	An overview of analyzing algorithms is provided in \refwra{sec:bg_ln-analysis}.

	With the available information, using techniques such as word sense disambiguation and dictionary lookup, it is generated an intermediate representation which can be a more or less complex semantic representation or Interlingua.
	From the intermediate format, language generation techniques are used, resorting once again to linguistic information such as dictionaries and grammars.
	
	The processes of analyzing a source language into an intermediate form and then generate the target language are called lexical and structural transfer.
	The transfer-based technique may employed in varying levels of analysis, being called \emph{deep} or \emph{superficial}.
	
	The formal grammars for parsing of the input parsing and language generation need to be designed by expert linguists. The logic for morphological analysis, dependency and semantic role identification also need rules done by experts.
	The employed dictionaries need to be fairly complete for the domain used.

	As it can be seen, although it requires no corpora, this method does require complete knowledge and expensive to build linguistic resources.


   \subsubsection{Formal Grammars}
   %what is
   Formal Grammars were devised with the purpose of defining the syntax of languages by using simple and precise mathematical rules.
   A grammar is composed by a set terminal and non terminal symbols, the start symbol and a set of production rules. Non terminal symbols are to be expanded further using the rules, leading to the start symbol when parsing or, when generating, to a set of grammatical correct sentence with terminal symbols only.
   Figure \ref{fig:cfg} illustrates this concepts with a Context Free Grammar, a popular grammar belonging to the class of Phrase Structure Grammars.
   
   	\begin{figure}[h]
   		\centering \includegraphics[scale=0.9]{translation/trees}
   		\caption{Example definition and generated tree of a Context Free Grammar}
   		\label{fig:cfg}
   	\end{figure}
 
   %uses
   Grammars are used to recognize and parse a sentence, being able to determine if it is a valid construction. Grammars are also used to generate language that is grammatically correct.

   %types
   Most formal grammars can be divided into main two categories: \emph{Phrase Structure} or \emph{Constituency Grammars} and \emph{Dependency Grammars}.
   
   Phrase Structure Grammars define a set of rules regarding the composition of a sentence and work on the principle of recursion, meaning that, a sentence can incorporate others ad eternum.
   
   Dependency Grammars see the verb as the central structure of a sentence, formulating all the other units as dependency to the verb. Dependencies may be os semantic, syntactical or morphological type.
   
   Other categories are \emph{Probablistic Grammars} and \emph{Transformational Grammars}.
   
   Formal Grammars are an intricate subject that should be done by linguists. It is out of the scope of this work to argue for the use of one or another in the context of \ac{LGP}.
   
   
   

	\subsubsection*{Statistical Machine Translation}
%		Word-based models translate word-to-word based on probability and then reorder the sentence based on alignment models.	Phrase-based models consider the context a word is inserted in.

	The \acf{SMT} process is defined as a translation system that automatically transfers the meaning of a source language sentence $ f_{1}^{J} = f_{1} \dots f_{J} $ into a target language sentence $ e_{1}^{I} = e_{1} \dots e_{I} $.
	
	The conversion itself is modeled as a problem of choosing the translation with the highest probability, denoted by $P( e_{1}^{I} | f_{1}^{J} )$.
	The mathematical modeling of this problem is defined in Eq. \ref{eq:argmax}.
	\begin{equation}\label{eq:argmax}
		\hat{e}_{1}^{I} = argmax_{e_{1}^{I}} \left\{ P(e_{1}^{I}|f_{1}^{J}) \right\}
	\end{equation}
	This is a global search algorithm for choosing a maximum value and it is a NP-hard problem, being the task of a \emph{decoder}.
	
	By applying Bayes' Theorem to the equation above, we get the result in Eq. \ref{eq:argmax-bayes}, where we omit the denominator as it is a constant regarding the maximization variable $e^J_1$.
	\begin{equation}\label{eq:argmax-bayes}
		\hat{e}_{1}^{I} = argmax_{e_1^I} \left\{ P(e_1^I) \cdot P(f_1^J|e_1^I) \right\}
	\end{equation}
	
	$P(e_1^I)$ is the \textbf{language model}, a model that yields the probability of a given sentence being observed in the target language. This probability is calculated by counting occurrences in a large, monolingual corpus.
	With the introduction of the Markov Assumption, that says that the probability of a word depends only on the previous ones, it is possible to look only at a limited history, also called context, before the word. This technique is named \textbf{Markov chains} or \textbf{n-grams}, where `n' is the number of words to look back in history.
	Equation \ref{eq:ngrams} shows the general mathematical definition of n-grams.
	\begin{equation}\label{eq:ngrams}
		P(e_1^I) \simeq \prod_{i=1}^I{P(e_i|e_{i-n} \dots e_{i-1})}
	\end{equation}
	
	Common terminology is \emph{bigram} for 2-grams and \emph{unigram} for 1-gram, where the later case counts only occurrences of the word itself in the corpora, not taking context into account.

	$P(f_1^J|e_1^I)$ is the \textbf{translation model}, the one responsible for establishing a correspondence between the source and target languages.
	This is accomplished by the use of a \textbf{lexicon model}, a statistical counting like the above, but over bilingual corpora that yields the most likely translation word by word, from the source to the target language. The translation model also needs an \textbf{alignment model} to determine the ordering and matching of sets of words between the pair of languages, as it frequently deviates from one to one word matchings.
	
	\begin{comment}
		\paragraph*{Smoothing Techniques}
		motivation: little corpus and 0 probabilities\\
		-Add-one / Laplace Smoothing\\
		-Backoff\\
		-Interpolation
				
		Perplexity measures how perplexed the system is when it finds a new word
	\end{comment}

		\vspace{0.5cm}
		A general introduction to \ac{SMT} can be found in \cite{nlpfoundations} and the companion website of Philipp Koehn's book\cite{smt-book} on \ac{SMT} has an organized bibliography\footnote{\url{http://www.statmt.org/book/bibliography}} with references to the history of \ac{MT}, non-statistical methods, statistical and hybrid approaches, software, competitions and research groups. The website is slightly outdated, but very well organized and still very informative. %The other pages on the website have more up-to-date information, including for the competitions and venues for research on \ac{MT}.



	\subsubsection{Hybrid approaches}
	The above mentioned approaches to \ac{MT} draw on each other to improve the final result.
	
	One possible way of mixing the approaches is by integrating statistical methods, specially the language model, into rule based systems (Knight et al, 1994; Habash and Dorr, 2002), or by using scoring functions to decide between generated alternatives (Carl, 2007).

	Another direction is to use statistical methods to learn the rules for use in rule based systems (Llitjós and Vogel, 2007). This technique is called example-based or machine learning. It is possible to use it in an unsupervised or human supervised way.

	\subsubsection{Tree-Based Models}
			Many translation problems are due to long dependencies or the reordering of elements in the sentence. These are weak spots in sequential, word-based, statistical models and are more easily addressed by taking syntax in account. For example, for the reordering, in some languages the rule is Noun+Adjective and in others Adjective+Noun.
			
			Tree-based models are based on a tree representation of the language by using formal grammars. The grammar rules are aligned for the language pair and are called \emph{synchronous grammar rules}. an example is:
			\begin{align*}
			&English: NP \rightarrow DET~JJ~NN\\
			&French: ~ NP \rightarrow DET~NN~JJ
			\end{align*}
			
			When using synchronous grammars,the input sentence is parsed, generating its deep structure in the form of a tree and, at the same time, the corresponding aligned tree is also generated.
			
			Synchronous grammars can be learned from word-aligned parallel corpora. \cite{chiang2005hierarchical, chiang2005hiero} describes an hierarchical phrase-based architecture that uses synchronous context-free grammars learned without any syntactic information.

	\subsubsection{Example-Based}

  \ac{EBMT}, a Machine Learning approach, uses parallel corpora to infer case by case rules for translating between the two languages.
  
  The types of algorithms to `learn' the rules fall into three categories: supervised, unsupervised and semi-supervised.

	The idea behind supervised learning is to gather positive and negative examples over a large collection. Examples of supervised learning techniques are Hidden Markov Models and the Maximum Entropy Model.
	
	For semi-supervised learning, the system needs to be \emph{bootstrapped} by being shown an initial set of examples. After this step, the algorithm can continue on itself, inferring new rules on top of the initially learned ones.
	
	Unsupervised learning has its basis on clustering of words and chunks, based on the similarity of the context.

	\subsubsection{Evaluation}
	%why evaluation is important
	Evaluation is a very important part of the \ac{MT} study as it allows to compare frameworks and measure improvements obtained when using or mixing different techniques.

	%evaluation is difficult -> multiple references + how is conducted
	Evaluation of Machine Translation is a hard problem due to linguistic variability and subjectivity. Even human translation can vary a lot, whether performed by experts or not.
	To account for variability, evaluation is typically conducted by having bilingual experts build a set of reference translations given a source sentence and then measure and rate the systems with different metrics.

	%metrics focused on the quality of the output
	Most \ac{MT} evaluation metrics focus on measuring the quality of the systems' output and not their usability, performance or other quality. Several methods that measure the comprehension and information extraction of the system have been attempted and dropped. 

	There are several possible ways to test a system, being human evaluation, automatic measures, task-based evaluation and custom oriented using hypothesis testing. For an overview of the benefits and shortcomings of these systems, see \cite{mt-eval}.
	Refer to \refwraa{app:mt_eval} for more details on each.

\begin{comment}

\cite{dgsthesis}

\cite{markov-over-phrase-based}
Phrase-based models (Koehn et al., 2003; Ochand Ney, 2004) learn local dependencies such as reorderings, idiomatic collocations, deletions and insertions by memorization. A fundamental drawback is that phrases are translated and reordered independently of each other and contextual information outside of phrasal boundaries is ignored.

The N-gram-based SMT framework addresses these problems by learning Markov chains over sequences of minimal translation units (MTUs) also known as tuples (Mariño et al., 2006) or over operations coupling lexical generation and reordering (Durrani et al., 2011). Because the models condition the MTU probabilities on the previous MTUs, they capture non-local dependencies and both source and target contextual information across phrasal boundaries.

In this paper we study the effect of integrating tuple-based N-gram models (TSM) and operation-based N-gram models (OSM) into the phrase-based model in Moses, a state-of-the-art phrase-based system. Rather than using POS-based rewrite rules (Crego and Mariño, 2006) to form a search graph, we use the ability of the phrase-based system to memorize larger translation units to replicate the effect of source linearization as done in the TSM model.


\end{comment}



