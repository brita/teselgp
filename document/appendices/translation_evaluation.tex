\chapter{Machine Translation Evaluation} \label{app:mt_eval}

	%human evaluation
	\paragraph{Human evaluation} is very costly and time consuming, being traditionally employed in \ac{MT} competitions. Besides the disadvantages, human testing is quite important as humans are the final target of the systems and are capable of subjective opinions and pattern recognizing that automatic systems are not. Typical measures in these evaluations are:
	\begin{description}
	\item[Adequacy] Does the output convey the same meaning as the input sentence? Is part of the message lost, added, or distorted?
	\item[Fluency] Fluency: Is the output well articulated? This involves both grammatical correctness and idiomatic word choices.
	\end{description}
	The output of these measures is a score on a subjective scale that is then averaged for each system, though it would not be strictly correct to compare these scores with those obtained in a different competitions, as the evaluators would vary in number and level of expertise and thus affect the results.
	Besides these measures, another popular form of evaluation is \textbf{ranking}, in which the evaluators order by suitability the obtained translations from different systems for a given source.

	%automatic methods
	\paragraph{Automatic methods} have the advantage of being inexpensive, fast and reproducible, allowing comparison of systems in different moments in time by using the same standard metrics.
	However, automatic metrics have several flaws and are still an open challenge.

	%what is a good metric
	Attributes for a good automatic metric are correlation with human opinion, sensitivity, consistency, reliability and generality. Generality means that it can be employed in different text domains and scenarios and consistency means to give similar results with the same system and similar text.

	%standard metrics
	\begin{description}
	\item[Precision, Recall and F-measure]
		Precision is a measure of retrieval of correct words only, while Recall measures how many of the correct words, among the existing ones, did the system manage to retrieve.
		\begin{equation} \label{eq:precision}
			Precision = \frac{TP}{TP + FP}
		\end{equation}\begin{equation} \label{eq:recall}
			Recall = \frac{TP}{TP + FN}
		\end{equation}
		Equations \ref{eq:precision} and \ref{eq:recall} define Precision and Recall, where $TP$ stands for \emph{True Positive}, $TN$ for \emph{True Negative}, $FP$ for \emph{False Positive} and $FN$ for \emph{False Negative}.
		The F-measure metric, given by the Equation \ref{eq:fmeasure}, relates Precision and Recall in a single score.
		\begin{equation}\label{eq:fmeasure}
			Fmeasure = \frac{(\beta^2 +1) * Precision * Recall}{\beta^2 * precision + Recall}
		\end{equation}

	\item[\acs{WER} (\acl{WER}) \acused{WER}]
		based on the calculation of the number of words that differ between the output and the reference translation. To calculate the difference, \ac{WER} uses the Levenshtein Distance:
		\begin{equation}
			WER = \frac{substitutions + insertions + deletions}{reference length}
		\end{equation}
		It is used both for speech recognition and translation systems.

	\item[\acs{PER} (\acl{PER}) \acused{PER}]
		similar to \ac{WER}, but allows for re-ordering of words and sequences of words between the output and the references.

	\item[\acs{BLEU} (\acl{BLEU}) \acused{BLEU}] \cite{bleu}
		uses modified n-gram precision to score a translated sentence against several references. The scores are averaged over the entire corpus to estimate the translation's overall quality, thus performing badly if used to evaluate individual segments or sentences. Equation \ref{eq:bleu} shows how to calculate this metric for 1 to 4 sized n-grams.
		\begin{equation} \label{eq:bleu}
			BLEU = min(1,\frac{output length}{reference length}) (\prod_{i=1}^{4} precision_i)^\frac{1}{4}
		\end{equation} 
		Intelligibility or grammatical correctness are not taken into account.
		\ac{BLEU} was one of the first metrics to achieve high correlations with human opinions and stands as the most popular.

	\item[\acs{METEOR} (\acl{METEOR}) \acused{METEOR}] \cite{meteor}
		based on the harmonic mean of unigram precision and recall, weighting recall higher than precision. Alignments are based on stemming, synonymy, paraphrase and exact word matching.
      
		This metric was designed to overcome some of the shortcomings of the \ac{BLEU} method. It has a better correlation with human judgment than \ac{BLEU} at the segment or sentence level while \ac{BLEU} seeks correlation at the corpus level.
	\end{description}

	\paragraph{Hypothesis Testing}
	Custom hypothesis about a system can be formulated and tested, for example, about the system's absolute score or if a change to the system is an improvement.
	The test is done with a statistical distribution, such as the Normal Distribution or the Student's t-distribution.

	A particularly useful test is for the comparison of two systems. Being formulated has 'is there a statistically significant difference in the results of the systems A and B'.
	The null hypothesis would be that there is no difference: $p_A = p_B = 0.5$. Using a binomial distribution, we can test if this hypothesis is true or false for a given confidence interval with Eq. \ref{eq:binom-comparison}.

	%$ p_A $ is the probability of the system A being better, and $p_B = 1 - p_A)$ the probability of system B being better.
	The probability of the system A being better on $k$ sentences out of a sample of $n$ is:
	\begin{equation} \label{eq:binom-comparison}
		\binom{n}{k} p^k (1-p)^{n-k} = \binom{n}{k}0.5^n = \frac{n!}{k!(n-k)!}0.5^n
	\end{equation}


	\paragraph{Task-based evaluation}
	In the case that the system is a means to an end, it can be useful to test how is the end purpose being achieved. As an example, in the context of this work, it is useful to test the level of understanding by humans of the avatar signing, instead of statistically testing an intermediate representation of the translation. Other examples besides communication assistance are content understanding post-editing machine translation.

